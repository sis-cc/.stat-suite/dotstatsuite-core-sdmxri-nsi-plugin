#---- sdmxri-nsi plugin build ---------------------------------------------------------
FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /app

ARG NUGET_FEED=https://api.nuget.org/v3/index.json

COPY DotStat.NSI.RetrieverFactory/*.csproj ./DotStat.NSI.RetrieverFactory/
COPY DotStat.NSI.DataRetriever/*.csproj ./DotStat.NSI.DataRetriever/
COPY version.json Directory.Build.props ./

RUN dotnet restore --source $NUGET_FEED DotStat.NSI.RetrieverFactory -p:WarningsAsErrors= -nowarn:NU1605

# copy everything else
COPY . ./
WORKDIR /app/DotStat.NSI.RetrieverFactory
RUN dotnet publish -c Release -o /out --no-restore -p:WarningsAsErrors= -nowarn:NU1605

#---- runtime ---------------------------------------------------------
FROM siscc/sdmxri-nsi-maapi:7.13.2-eaf43315 as runtime

WORKDIR /app

#-- copy plugin binaries
COPY --from=build /out/DotStat.* /app/Plugins/

COPY --from=build /out/System.Data.SqlClient.dll /app/Plugins/

#-- copy localization.json
COPY --from=build /out/config/localization.json /app/config/

ENTRYPOINT ["./entrypoint.sh"]
