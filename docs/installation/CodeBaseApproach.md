# Installing DotstatSuite-Core-Sdmxri-Nsi-Plugin service with the Codebase approach

## Purpose of this document
The goal of this document to assist you setting up dotstatsuite-core-sdmxri-nsi-plugin using the codebase approach.

## Content of this document
- Prerequisites
- Database
- Service
- Configuration

## Prerequisites
- IIS server 7.5 or later
- dotnet-hosting-3.1.3-win installed on web server
- Related NSI webservice (Build or download it)

## Database
### Structure database 
Install/update the structure database related to the NSI webservice. 

### Data database
Follow the instructions install a **DotStatSuiteCore Data Database** [See steps](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/blob/master/docs/installation/CodeBaseApproach.md)
.

## Service
Refer to the [nsiplugin changelog](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/-/blob/master/CHANGELOG.md) and [NSIWebservice changelog](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/-/blob/master/CHANGELOG_NSIWS.md) for the versions of the dotstatsuite-core-sdmxri-nsi-plugin and the NSIWebservice.

### Build the plugin
Compile and publish the solution with latest visual studio 2019, or run the "dotnet publish" command in the root folder of the dotstatsuite-core-sdmxri-nsi-plugin repository.

Take the following binaries from the publish folder. By default it should be : "c:\git\dotstatsuite-core-sdmxri-nsi-plugin\DotStat.NSI.RetrieverFactory\bin\Debug\netcoreapp3.1\publish\"
- DotStat.Common.dll
- DotStat.Config.dll
- DotStat.DB.dll
- DotStat.Domain.dll
- DotStat.MappingStore.dll
- DotStat.NSI.DataRetriever.dll
- DotStat.NSI.RetrieverFactory.dll
- DotStat.NSI.RetrieverFactory.deps.json

Copy these files to the Plugins folder of the NSI Webservice.

### Update existing webservice
If you already have a previous version of NSIWebservice in an existing .Stat Suite deployment, you can update the existing NSIWebservice:
- Please create a backup of the folder of your existing NSIWebservice.
- Remove all files from the folder of your existing NSIWebservice.
- Copy the new version of NSIWebservice to the folder.
- Copy the built files to the NSIWebservice's Plugins folder.


### Install new webservice
If you are installing a new instance of NSIWebservice, please follow these steps:
- Create the folder for NSIWebservice on the Web hosting machine. 
- Copy the built binaries to the NSIWebservice's Plugins folder.
- Open Internet Information Service on the the Web hosting machine.
- Create Application pool for NSIWebservice. 
- Set application pool properties. the value for ".Net Clr version" should be "No managed code"
- Create application for NSIWebservice. Use the application pool created in previous step for application pool. Use the folder of the NSIWebservice for Physical path.
- Open file explorer, and edit the properties of the folder containing the NSIWebservice. 
- Grant full control access for the local machine's IUSR and IIS_IUSRS users.

## Configuration
The configuration files of the NSIWebservice are in the config folder inside the NSIWebservice folder.
### App.config
Open the app.config file and do the following changes:
- Remove 
```
    <sectionGroup name="estat.nsi.ws.config">
      <section name="auth" type="Estat.Nsi.AuthModule.AuthConfigSection, Estat.Nsi.AuthModule"/>
    </sectionGroup>
```
- Replace: 
``` 
    <!-- Replace HOST with hostname or IP address of the server/PC running mapping store database -->
    <!-- Replace MADB with Mapping store database -->
    <!-- Replace USER with Mapping store database user name -->
    <!-- Replace PASS with Mapping store database user password -->
    <!-- Uncomment only one <add.../> at a time.-->
    <!-- Sql Server without any instance-->
    <!--<add name="MappingStoreServer" connectionString="Data Source=HOST;Initial Catalog=MADB;Integrated Security=False;User ID=USER;Password=PASS"
     providerName="System.Data.SqlClient" />-->
    <!-- Sql Server Express-->
    <!--<add name="MappingStoreServer" connectionString="Data Source=HOST\sqlexpress;Initial Catalog=MADB;Integrated Security=False;User ID=USER;Password=PASS"
     providerName="System.Data.SqlClient" />-->
    <!-- Sql Server Express on localhost using integral security-->
    <!--<add name="MappingStoreServer" connectionString="Data Source=HOST\sqlexpress;Initial Catalog=MADB;Integrated Security=False;User ID=USER;Password=PASS"
     providerName="System.Data.SqlClient" />-->
    <!-- Oracle Express using the Oracle ODP.NET provider -->
    <!--<add name="MappingStoreServer" connectionString="Data Source=HOST/MADB;User ID=USER;Password=PASS"
         providerName="Oracle.ManagedDataAccess.Client" />-->
    <!-- MySQL -->
    <!--<add name="MappingStoreServer" connectionString="server=HOST;user id=USER;password=PASS;database=MADB" providerName="MySql.Data.MySqlClient"/>-->
    <!-- The same applies to Authorization/Authentication database. Default name : 'authdb' but can be changed at dbAuth config below-->
    <!-- Example of using authtest db on localhost sqlexpress db-->
    <!--<add name="authdb" connectionString="Data Source=.\SQLEXPRESS;Initial Catalog=authtest;Integrated Security=False;User ID=USER;Password=PASS" providerName="System.Data.SqlClient" />-->
```
To 
```
<add name="MappingStoreServer" connectionString="Data Source={yourDBServer};Initial Catalog={YourStructureDatabase};User Id={YourStrucuterDatabaseWriteruser};Password={YourStrucuterDatabaseWriterpassword};" providerName="System.Data.SqlClient"/>     
```
- Replace
```
<value>SOME_NSI</value>
```
To
```
<value>.Stat V8</value>
```
- Replace
```
<add key="enableSubmitStructure" value="false"/>
```
To
```
<add key="enableSubmitStructure" value="true"/>
```
- Replace
```
<add key="configLocation" value="c:\ProgramData\Eurostat\nsiws.config"/> 
```
To
```
<!--<add key="configLocation" value="c:\ProgramData\Eurostat\nsiws.config"/>--> 
```
- Replace
```
<add key="corsSettings" value="false"/>
```
To
```
<add key="corsSettings" value="true"/>
```
- Replace
```
	<add key="estat.sri.ws.policymodule" value="true"/>
    <!--A comma separated string that determines the order for the middleware implementations -->
    <add key="middlewareImplementation" value="CorsMiddlewareBuilder,AuthMiddlewareBuilder,PolicyModuleMiddlewareBuilder"/>
```	
To
```
    <add key="estat.sri.ws.policymodule" value="false"/>
    <add key="middlewareImplementation" value="CorsMiddlewareBuilder,OpenIdMiddlewareBuilder"/>

```
- Remove
```
<estat.nsi.ws.config>
    <!-- authentication configuration-->
    <auth anonymousUser="" realm="nsiws">
      <userCredentialsImplementation type="UserCredentialsHttpBasic"/>
      <!-- The AUTH DB provider. It looks for a connection string with name equal to "authdb"-->
      <authenticationImplementation type="EstatSriSecurityAuthenticationProvider"/>
      <!-- Legacy options that use the Auth Schema and Test Auth config -->
      <!--
      <authenticationImplementation type="DbAuthenticationProvider"/>
      <authorizationImplementation type="DbAuthorizationProvider"/>
      <dbAuth>
        <authentication sql="select password from users where userid=${user}" connectionStringName="authdb"/>
        <authorization sql="select d.id as ${id}, d.ver as ${version}, d.agency as ${agencyId} from users u inner join user_df ud on u.pk=ud.user_pk inner join dfs d on d.pk = ud.df_pk where u.userid=${user}"/>
      </dbAuth>
      -->
    </auth>
  </estat.nsi.ws.config>
```
- Replace
```
    <corsCollection>
      <!-- Allow CORS to any caller, to restrict change * to allowed domain, if multiple allowed domains can access NSI webservice then use as many add nodes as needed -->
      <!-- <add domain="http://localhost:3030" allowed-methods="GET,POST" allowed-headers="Range" /> -->
      <!-- <add domain="*" allowed-methods="GET,POST,PUT" allowed-headers="Range, Origin, Authorization" allow-credentials="true" exposed-headers="Location" /> -->
    </corsCollection>
```
To
```
    <corsCollection>
        <add domain="*" allowed-methods="GET,POST,PUT" allowed-headers="Range, Origin, Authorization" allow-credentials="true" exposed-headers="Location" />
    </corsCollection>
```
- Replace
```
    <retrievers>
      <!-- First provider to be tried -->
      <add name="MappingStoreRetrieversFactory"/>
      <!-- Second provider to be tried. It will tried only if first cannot serve the result.-->
      <!-- <add name="SomeFactoryId"/>-->
      <!-- Third -->
      <!-- etc. -->
    </retrievers>
```
To
```
    <retrievers>
        <add name="DotStatRetrieverFactory"/>
    </retrievers>
```
Save the file
### dataspaces.private.json
Create a new file in the config folder called dataspaces.private.json
Edit the content. It should be like:
```
{   
    "DotStatSuiteCoreCommonDbConnectionString": "Data Source={YourDataDatabaseServer};Initial Catalog={YourDatabase};User ID={YourDbUsername};Password={YourDbPassword}",
    "spacesInternal": [
    {
      "Id": "design",      
      "DotStatSuiteCoreStructDbConnectionString":"Data Source={YourDataDatabaseServer};Initial Catalog={YourdesignStructureDatabase};User Id={YourdesignStrucuterDatabaseWriteruser};Password={YourdesignStrucuterDatabaseWriterpassword};",
      "DotStatSuiteCoreDataDbConnectionString": "Data Source={YourDatabaseServer};Initial Catalog={YourdesignDataDatabase};User Id={YourdesignDataDatabaseWriteruser};Password={YourdesignDataDatabaseWriterpassword};", 
      "DataImportTimeOutInMinutes": 60,      
      "DatabaseCommandTimeoutInSec": 60
    }
  ]
}
```
Save the file

### localization.json

Original version of this file can be  found in config subfolder of the plugin's publish folder (the target folder where the plugin binaries were built to). If you don't already have custom localizations then copy this file to the config folder of the NSIWebservice. If you have customized localizations, the please check if there are changes (new or removed entries or updated localized strings) in the localization.json file in publish folder an apply them on your customized localization file.

The content of this file should be like:
```
{
  "LocalizedResources": {
    "en": {
      "LinkToDocumentation": "https://gitlab.com/snippets/1908874",
      "MoreDetails": "Details",
      "MailSubject": "Data Lifecycle Manager transfer request ID - '{0}', target space '{1}'",
      "MailBody": "<p>Dear DLM user, your transfer has been processed in the system, please see below the details:</p><div><h3>Summary</h3>{0}</div><h3>Transaction messages</h3><table style=\"padding:5px\"><tr><th>Date</th><th>Level</th><th>Message</th></tr>{1}</table>",
      "EmailSummary": "<ul><li><strong>Request ID:</strong> {0}</li>{1}<li><strong>Destination data space:</strong> {2}</li><li><strong>Dataflow:</strong> {3}</li><li><strong>User:</strong> {4}</li><li><strong>Request status:</strong> {5}</li></ul>",
      "EmailSummarySourceDataSpace": "<li><strong>Destination data space:</strong> {0}</li>",
      "ObservationsProcessed": "{0} observations were processed {linkDetails}",
      "NoObservationsProcessed": "No observations were processed {linkDetails}",
      "SubmissionResult": "Successfully registered request with ID: {0}",
      "DataflowLoaded": "Successfully loaded structure information of dataflow '{0}' from dataspace '{1}'.",
      "StreamingFinished": "Streaming succeeded.",
      "PluginProcessFinished": "Successfully verified/transformed observations.",
      "TransactionAbortedDueToConcurrentTransaction": "Aborting current transaction with ID {0}. There is already an active transaction in progress for DSD ID {1} with transaction ID {2}.{linkDetails}",
      "PreviousTransactionTimedOut": "The previous transaction (ID {0}) targeting the same DSD ID {1} timed out so has been terminated.{linkDetails}",
      "TransactionSuccessful": "Transaction closed successfully.",
      "NoActiveTransactionWithId": "There is no active transaction to close with ID {0}.{linkDetails}",
      "TerminatingTimedOutTransaction": "Terminating timed out transaction ID {0}).{linkDetails}",
      "CodeNotFoundForDimension": "Code [{0}] not found for dimension {1}.{linkDetails}",
      "DimensionNotFoundByCodeListTranslator": "Dimension code [{0}] not found by code list translator.{linkDetails}",
      "DataflowNotFoundInDataspace": "Dataflow [{0}:{1}({2})] not found in [{3}] dataspace.{linkDetails}",
      "DimensionDoesNotBelongToDataset": "The dimension {0} does not belong to dataset {1}.{linkDetails}",
      "AttributeDoesNotBelongToDataset": "The attribute {0} does not belong to dataset {1}.",
      "MissingCodeMember": "Missing member for [{3}] dimension, at row [{0}].{linkDetails}",
      "UnknownCodeMember": "Unknown member for [{3}], at row [{0}].{linkDetails}",
      "ConstraintViolation": "Constraint violation for [{3}], at row [{0}].{linkDetails}",
      "NullValue": "Mandatory attribute / observation value empty, Coordinate: [{1}], at row [{0}].{linkDetails}",
      "InvalidValueFormat": "Observation value format error [{2}], at row [{0}]{linkDetails}",
      "DuplicatedCoordinate": "Dublicate coordinate [{1}], at row [{0}].{linkDetails}",
      "TransactionAbortedDueToConcurrentArtefactAccess": "Aborting transaction due to concurrent process creating the same artefact: {0}.{linkDetails}",
      "ChangeInDsdCommonMessage": "Change has been detected on DSD {0} since the last dataload.{linkDetails}",
      "ChangeInDsdDimensionToTimeDimension": "Dimension changed to time dimension: {0}.{linkDetails}",
      "ChangeInDsdDimensionAdded": "New dimension added: {0}.{linkDetails}",
      "ChangeInDsdDimensionRemoved": "Dimension(s) removed: {0}.{linkDetails}",
      "ChangeInDsdDimensionCodelistChanged": "Code list of dimension {0} changed to {1}.{linkDetails}",
      "ChangeInDsdTimeDimensionAdded": "Time dimension has been added to the DSD.{linkDetails}",
      "ChangeInDsdTimeDimensionRemoved": "Time dimension has been removed from the DSD.{linkDetails}",
      "ChangeInDsdAttributeAdded": "New attribute added: {0}.{linkDetails}",
      "ChangeInDsdAttributeCodedRepresentationChanged": "Representation of attribute {0} changed from {1} coded to {2} coded.{linkDetails}",
      "ChangeInDsdAttributeMandatoryStateChanged": "Attribute {0} changed from {1} to {2}.{linkDetails}",
      "ChangeInDsdAttributeCodelistChanged": "Code list of attribute {0} changed to {1}.{linkDetails}",
      "ChangeInDsdAttributeAttachmentLevelChanged": "Attachment level of attribute {0} changed from {1} to {2}.{linkDetails}",
      "ChangeInDsdAttributeAttachmentGroupChanged": "Attachment group of attribute {0} changed from {1} to {2}.{linkDetails}",
      "ChangeInDsdAttributeRemoved": "Attribute(s) removed: {0}.{linkDetails}",
      "ChangeInDsdDimensionCodelistCodeRemoved": "Code(s) removed from codelist {0} of dimension {1} : {2}.{linkDetails}",
      "ChangeInDsdAttributeCodelistCodeRemoved": "Code(s) removed from codelist {0} of attribute {1} : {2}.{linkDetails}",
      "ChangeInDsdDimensionCodelistCodeAdded": "Code(s) appended to codelist {0} of dimension {1} : {2}.{linkDetails}",
      "ChangeInDsdAttributeCodelistCodeAdded": "Code(s) appended to codelist {0} of attribute {1} : {2}.{linkDetails}",
      "FailedToOpenDatastoreDbConnectionForCleanup": "Failed to open datastore database [{0}] connection for cleanup of transaction [{1}]. Please contact your system administrator.{linkDetails}",
      "FailedToOpenDatastoreDbConnection": "Failed to open datastore database [{0}] connection. Please contact your system administrator.{linkDetails}",
      "DatabaseTableNotFound": "Table {0} not found in database. Please contact your system administrator.{linkDetails}",
      "AttributeNotDefinedInDsd": "Attribute {3} not defined in DSD {4}.{linkDetails}",
      "AttributeReportedAtMultipleLevels": "Attribute {3} has values reported at observation and non-observation levels. Attribute values should be reported only at one attachment level.{linkDetails}",
      "MandatoryDatasetAttributeWithNullValue": "Attribute {3} is mandatory but empty value is provided.{linkDetails}",
      "MandatoryAttributeWithNullValue": "Attribute {3} is mandatory but empty value is provided at key {1}.{linkDetails}",
      "UnknownAttributeCodeMemberWithoutCoordinate": "Unknown code for attribute {3} : {4}.{linkDetails}",
      "UnknownAttributeCodeMemberWithCoordinate": "Unknown code for attribute {3} at key {1} : {4}.{linkDetails}",
      "DuplicatedDatasetAttribute": "Values provided multiple times for attribute {3}.{linkDetails}",
      "DuplicatedAttributeKey": "Values provided multiple times for attribute {3} at key {1}.{linkDetails}",
      "WrongSetOfDimensionsInKeyOfKeyable": "Attribute {3} is reported at a keyable where the partial keys are not matching the attribute definition in DSD. Key {4} reported but combination of {5} expected.{linkDetails}",
      "MissingCodeMemberWithCoordinate": "Missing code for for component {3} at key {1}.{linkDetails}",
      "UnknownCodeMemberWithCoordinate": "Unknown code for component {3} at key {1} : {4}.{linkDetails}",
      "UnknownCodeMemberWithoutCoordinate": "Unknown code for component {3} : {4}.{linkDetails}",
      "ObservationAttributeReportedAtDataset": "Observation level attribute {3} reported at dataset level. Should be reported at observation level.{linkDetails}",
      "ObservationAttributeReportedAtKeyables": "Observation level attribute {3} reported at non-observation level at key {1}. Should be reported at observation level.{linkDetails}",
      "DatasetAttributeReportedAtKeyables": "Dataset level attribute {3} reported at dimension/group level at key {1}. Should be reported at dataset or observation level.{linkDetails}",
      "DimGroupAttributeReportedAtDataset": "Dimension/group level attribute {3} reported at dataset level. Should be reported at dimension/group or observation level.{linkDetails}",
      "MandatoryDatasetAttributeMissing": "{3} is mandatory dataset attribute but has no value provided yet.{linkDetails}",
      "MandatoryDimGroupAttributeMissing": "Mandatory attribute {3} has no value provided but observation data is being loaded for coordinate: {1}.{linkDetails}",
      "MandatoryAttributeWithNullValueAndExistingObservationData": "Mandatory {3} attribute value cannot be deleted due to related observation data existing in database for coordinate: {1}.{linkDetails}",
      "MandatoryAttributeWithNullValueAndDataInStaging": "Mandatory {3} attribute value cannot be deleted due to observation data being loaded for the same coordinate: {1}.{linkDetails}",
      "TextAttributeValueLengthExceedsMaxLimitWithCoordinate": "Length of {3} attribute's value exceeds the limit ({5} > {6}) at coordinate {1}: {4}.{linkDetails}",
      "TextAttributeValueLengthExceedsMaxLimitWithoutCoordinate": "Length of {3} attribute's value exceeds the limit ({5} > {6}): {4}.{linkDetails}",
      "SdmxSourceHttpRequestException": "Failed to open external SDMX source URL: {0}.",
      "MaximumTextAttributeLengthDecreaseNotSupported": "Decreasing value of 'Maximum text attribute length' parameter is not supported. Current limit: {0}, requested limit: {1}",
      "MaximumTextAttributeLengthNewValueApplied": "The new 'Maximum text attribute length' value {0} has been applied on related database tables of DSD {1}.",
      "MaximumTextAttributeLengthInvalidDSDAnnotationValue": "Value set at MaxTextAttributeLength annotation of DSD {0} is invalid: {1}"
    }
  }
}

```
Save the File

### auth.json

Create a new file in the `config` folder called `auth.json`, meaning of every field is described [here](../../readme.md#authenticationauthorization)
Edit the content. It should be like:

```json
{
  "auth": {
    "enabled": true,
    "allowAnonymous": false,
    "authority": "AUTHORITY URL",
    "clientId": "VALUE",
    "requireHttps": true,
    "validateIssuer": true,
    "showPii": false,
    "claimsMapping": {
        "email": "email",
        "groups": "groups"
    }
  }
}
```

Save the File

Test the application:
- open your NSIWebservice in a browser, and check the main page appears
- Try to run a dataflow query against the database like /rest/dataflow
