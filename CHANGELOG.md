# HISTORY

## v7.13.2.1 (data database v2.1 and Mappingstore db v6.10)
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/72
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/73

## v7.13.2 (data database v2.1 and Mappingstore db v6.10)
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/66
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/59
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/46
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/142
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/38
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/33


## v7.13.0.1 (data database v2.1 and Mappingstore db v6.10)
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/65
    
## v7.13.0 (data database v2.1 and Mappingstore db v6.10)
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/53
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/50
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-kube-core-rp/-/issues/12
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/49
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/-/issues/8
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/29

## v7.12.1 2020-06-09 (data database v2.1 and Mappingstore db v6.9)

Uses new version of the Eurostat NSIWS v7.12.1

- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/41
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/93
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/-/issues/47

##  v7.11.3 2020-02-27 (Uses databases v2.1)

- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/-/issues/42*
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/34
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/25
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/42
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/23
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/-/issues/34
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/30
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/28
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/21
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/32
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/-/issues/45

* Important change: Docker image is temporarily using different base image

##  v7.11.1 2020-02-13 (Uses databases v2.1)

- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/issues/36
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/issues/22
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/issues/41
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/issues/40
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/issues/13
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/issues/40
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/issues/14
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/issues/7
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/issues/39

##  v7.10.10 2020-01-31 (Uses databases v2.1)

- sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/issues/15
- sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/issues/35


##  v7.10.8 2020-01-29 (Uses databases v2.1)
This release contains breaking changes with changes to the authentication management.

- sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/issues/12
- sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/issues/27
- sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/issues/24
- sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/issues/12
- sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/issues/19
- sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/issues/25
- sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/issues/22

##  v7.11.1 2020-02-13 (Uses databases v2.1)

- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/issues/36
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/issues/22
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/issues/41
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/issues/40
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/issues/13
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/issues/40
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/issues/14
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/issues/7
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/issues/39

##  v7.10.10 2020-01-31 (Uses databases v2.1)

- sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/issues/15
- sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/issues/35


##  v7.10.8 2020-01-29 (Uses databases v2.1)
This release contains breaking changes with changes to the authentication management.

- sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/issues/12
- sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/issues/27
- sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/issues/24
- sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/issues/12
- sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/issues/19
- sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/issues/25
- sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/issues/22