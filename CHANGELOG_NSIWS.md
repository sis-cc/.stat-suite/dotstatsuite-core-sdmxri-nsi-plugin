# HISTORY

## Important  changes in v7.x.x

1. Starting from 7.0.0 NSIWS targets .NET Core and require ASP.NET Hosting module to work with IIS
   1. Up to version 7.10.x targets .NET Core 2.2
   1. Since version 7.11.0 targets .NET Core 3.1
1. Each `NSIWS` instance requires a separate Application Pool with target : 'No Managed'
1. `Web.Config` contains only the IIS Configuration needed for running `NSIWS` on IIS
1. The rest of the configuration resides now in [Main configuration file](doc/CONFIGURATION.md#main-configuration-file) and `config/*.json` files

*WARNING* Currently NSI WS will try to copy `config/app.config` if it exists to `NSIWebServiceCore.dll.config` everytime the application pool starts or recycles.
To avoid this behavior remove the `config/app.config` or move it to `NSIWebServiceCore.dll.config`

## nsiws.net v7.13.2 (2020-09-02) (MSDB v6.10) (AUTHDB v1.0)

### Details v7.13.2

1. SDMX v2.1 data message Header can reference now either a dataflow or a DSD. This can change in `config/sdmx-header.json`. The default is `dataflow`
1. It is possible to receive a zip file by setting `zip=true` in the Accept HTTP header (From OECD)
1. Experimental feature. To workaround URL size limitations it is possible to `POST` the key in the body in REST data queries. See [doc/EXTENDED_API](doc/EXTENDED_API.md)

### Tickets v7.13.2

The following new features added:

- SDMXRI-1380: REST Data queries and URL size limitations (.NET) (ISTAT,future-QTM)
- SDMXRI-1414: NSIWS make StructureUsage configurable (.NET) (PUBLIC_2020,future-QTM)
- SDMXRI-1335: NSIWS Zip file download feature (OECD,PULL_REQUEST,QTM6-2019.0281)

## nsiws.net v7.13.1 (2020-07-23) (MSDB v6.10) (AUTHDB v1.0)

### Important changes v7.13.1

#### Non backwards compatible changes/breaking changes v7.13.1

SDMX-JSON message output has changed.

##### meta content languages changes in all messages

`meta.content-languages` renamed to `meta.contentLanguages` in structure and data messages

##### links array changes in structure messages

- empty links array is never written
- `link.type` is never written
- `link.urn` is only written if the request had an accept header: `"urn=true"`
- `link.rel` possible types
  - `self`: only added if `"urn=true"` is defined
  - `external`: only added if `isExternalReference=true`
  - `href` is populated with structureURL
  - `urn` only added if `"urn=true"` is defined
  - `structure`: only added if `"urn=true"` is defined and the artefact has a DSD or MSD cross-reference
  - direct `"urn"` artefact property is removed as it's not part of the standard and the same information can be found now in the links array with `"self"` `link.rel` type

### Details v7.13.1

1. SDMX-CSV and SDMX-JSON improvements from OECD
1. Fix issue ignoring observation values when reading SDMX v2.1 Flat Generic Data from OECD
1. Support for Hierarchical codelists in SDMX-JSON from OECD
1. Various bugfixes, including
   1. Improved Provision agreement import/retrieval support, Metadaflow and better error reporting
   1. Couldn't use/save MariaDB DDB connections when a port was used

### Tickets v7.13.1

The following new features added:

- SDMXRI-1368: Add Hierarchical codelist support to JSON structure message  (OECD,PULL_REQUEST)

The following bugs have been corrected:

- SDMXRI-1402: CsvDataReaderEngine to handle data with ids+labels along with freetext attributes having ": " (OECD,PULL_REQUEST)
- SDMXRI-1397: Optional attributes are NOT required when importing data (not working for CSV import) (OECD,PULL_REQUEST)
- SDMXRI-1367: Fix JSON format: contentLanguages, links (OECD,PULL_REQUEST)
- SDMXRI-1384: GenericDataReaderEngine doesn't read observation value from flat data XML (OECD,PULL_REQUEST)

## nsiws.net v7.13.0 (2020-07-02) (MSDB v6.10) (AUTHDB v1.0)

### Details v7.13.0

1. Included a new document that contains [IIS common issues and solutions](doc/IIS_WORKAROUNDS.md)
1. New health source for checking the MSDB details from OECD.

### Tickets v7.13.0

The following new features added:

- SDMXRI-1330: SDMX_RI Rest issues (3rd-level-support)
- SDMXRI-1379: Add mapping store database version to a healthcheck (OECD, PULL_REQUEST)

## nsiws.net v7.12.2 (2020-06-12) (MSDB v6.10) (AUTHDB v1.0)

### Details v7.12.2

1. Target MSDB v6.10
1. Added SQL Server docker file that starts an empty DB and an AUTHDB
1. Check if a request has a `X-Requested-With` HTTP request header in order not to include the HTTP response `WWW-Authenticate`.
1. Support for `Accept-Encoding: application/zip` to download structure/data in zip file.
1. Category scheme enhancements from OECD:
   1. Manage hierarchies of items in the queries using dots (see https://github.com/sdmx-twg/sdmx-rest/blob/master/v2_1/ws/rest/docs/4_3_structural_queries.md)
   1. The same category (with the same id) can appear on multiple nodes in the hierarchy
   1. Manage "references=dataflow" properly
1. Support for Ignore Production Flag for data.
1. Concept core representation support, text format and Codelist

### Tickets v7.12.2

The following new features added:

- SDMXRI-1366: MAWS.NET : Delete the popup that asks to sigin (QTM6-2019.0281)
- SDMXRI-1125: Provide Linux-based Docker image for NSI web service + MSDB/AuthDB (OECD,QTM2-2019.0281) (PARTIAL)
- SDMXRI-1335: NSIWS Zip file download feature (OECD, PULL_REQUEST)
- SDMXRI-1361: Add CSV support to SdmxDataReaderFactory (OECD,PULL_REQUEST)
- SDMXRI-1308: Manage hierarchical categoryschemes (OECD,QTM6-2019.0281, PULL_REQUEST)
- SDMXRI-1219: Restore the ignoreProductionFlagForData parameter (.NET) (ISTAT,QTM6-2019.0281)
- SDMXRI-1265: Support Concept Scheme Core representation store/retrieval/references (.NET) (OECD,QTM6-2019.0281)
- SDMXRI-1255: MAWEB: Registries WS listed (QTM6-2019.0281)
- SDMXRI-1365: MSDB add name unique constraints for DDB, Dataset tables (QTM6-2019.0281)

## nsiws.net v7.12.1 (2020-05-21) (MSDB v6.9) (AUTHDB v1.0)

### Tickets v7.12.1

The following new features added:

- SDMXRI-1281: SDMX v2.1 format, use StructureUsage (.NET) (OECD,QTM6-2019.0281)


## nsiws.net v7.12.0 (2020-05-13) (MSDB v6.9) (AUTHDB v1.0)

### Details v7.12.0

1. In SDMX v2.1 DSD it is now possible for an attribute to attach to the Time Dimension
1. Support for groups in SDMX-CSV
1. Only one content constraint of the same type that is attached to the same artefact can apply to a specific component.
   1. Validity dates are considered
1. Multiple enhancement to the maapi tool (Estat.Sri.Mapping.Tool.exe)
   1. Pass connection string in command line
   1. New option for upgrade command to initialize the MSDB if it is not initialized
   1. Better error reporting
1. Changes in Dockerfile to produce smaller images (and faster)

### Tickets v7.12.0

The following new features added:

- SDMXRI-1125: Provide Linux-based Docker image for NSI web service + MSDB/AuthDB (OECD,QTM2-2019.0281)
- SDMXRI-1172: Apply rules for multiple content constraints (of same type) attached to same artefact (OECD,QTM2-2019.0281)
- SDMXRI-1123: Enhancements for command line MAAPI Tool (OECD,QTM2-2019.0281)
- SDMXRI-1298: NSI WS recognises attributes attached to TIME_PERIOD as observation level attributes (.NET) (OECD,QTM6-2019.0281)
- SDMXRI-1224: SDMX-CSV data writer support for groups (.NET) (OECD,QTM2-2019.0281)
- SDMXRI-1281: SDMX v2.1 format, use StructureUsage (.NET) (OECD,QTM6-2019.0281)

## nsiws.net v7.11.5 (2020-04-09)  (MSDB v6.9) (AUTHDB v1.0)

### Known issues v7.11.5

Reproduced so far only under IIS (in Docker) and SOAP UI.
Some SOAP/REST calls might return an empty response with HTTP header 200 although no error is found in the logs. Repeating the call returns the correct response.
It is not clear yet if it is SOAP UI in fault or NSIWS or IIS.

### Important changes v7.11.5

1. Affects REST calls and POST/PUT methods
   1. The status code for updating successfully non-final SDMX artefacts that have dependencies has changed from `403` to `200`.
   1. The status code for inserting and updating multiple SDMX artefacts has changed from `207` to `201`

### Details v7.11.5

1. Fix issue with nuget package references. Some package reference versions were wildcarded causing inconsistent behavior between builds
1. Delay calls to MSDB from format plugins only when they are needed
1. Automatic Category creation is controlled by configuration. See [CONFIGURATION](config/Properties.json)
1. Do not report as error/warning updates to non-final SDMX artefacts
1. Do not report as multi-status the combination of successful insert/updates to SDMX artefacts

### Tickets v7.11.5

The following new features added:

- SDMXRI-1110: Make automatic Category creations from categorisation configurable (OECD,QTM2-2019.0281)
- SDMXRI-1311:  Data query performance issue with nsiws (.NET) (ILO,OECD)

The following bugs have been corrected:

- SDMXRI-1315: Inconsistent nuget package reference across NSI solution
- SDMXRI-1267: Not possible to update names/annotations in non-final artefacts that are referenced by other artefacts (OECD)

## nsiws.net v7.11.4 (2020-03-19) (MSDB v6.9) (AUTHDB v1.0)

### Important changes v7.11.4

#### Changes that affect backward compatibility (V7.11.4)

The default logging configuration has changed:

1. User vs System activity
1. Different filenames
1. Dataflow activity disabled now by default

Please consult the [CONFIGURATION](doc/CONFIGURATION.md)

### Details v7.11.4

1. Default logging configuration changed. Logging of user and system activity logged in different files
1. Target MSDB v6.9
   1. Annotation store procedure title parameter size increased (OECD)
   1. Dataflow table new nullable field for linking to Data source table
1. Fixes related to data/structure validation functionality in code re-used by MAWS
1. CORS error handling fixes from OECD
1. New language code support `Ladin Dolomitan`.

### Tickets v7.11.4

The following new features added:

- SDMXRI-1118: Modify default log4net configuration. (OECD,QTM2-2019.0281)
- SDMXRI-968: Implementation of Data Registation/monitor: Configure data source per dataflow (SQL) (QTM2-2019.0281)
- SDMXRI-1206: SDMX validation inside the Mapping Assistant (MA_WEB) (QTM2-2019.0281)
- SDMXRI-1310: Support `lld` ISO-639-3 Language code in SdmxSource.NET (ISTAT)

The following bugs have been corrected:

- SDMXRI-1290: Annotation Title is still fixed to 70 characters
- SDMXRI-1302: headers written in Cors middleware are not present in the http response in case of exception and it's treatment with ExceptionHandler

## nsiws.net v7.11.3 (2020-03-02) (MSDB v6.8) (AUTHDB v1.0)

### Details v7.11.3

1. Fix regression that caused the Web Service to return no results when a query was not for a datastructure

### Tickets v7.11.3

The following bugs have been corrected:

- SDMXRI-1297: Cannot query artefacts other than datastructure

## nsiws.net v7.11.2 (2020-02-27) (MSDB v6.8) (AUTHDB v1.0)

### Important changes v7.11.2

#### Changes that affect backward compatibility (V7.11.2)

1. Starting from this version NSIWS will allow querying data and structure of a SDMX v2.0 CrossSectional DSD in SDMX v2.1 formats.
   1. Previous versions of NSIWS would return Data Structure Definitions (DSD) with SDMX v2.0 CrossSectional attachment flags as stubs in SDMX v2.1 XML Structure format and previous versions of NSIWS would not allow to retrieve data in SDMX v2.1 XML Format for such DSD.
   1. It is possible to revert to the old behavior by changing or removing the `config/Properties.json` file, see the [CONFIGURATION](doc/CONFIGURATION.md) for more details.
1. Changes in non-final artefact behavior. Starting from this version it is possible to update existing non-final artefacts that are referenced by other artefacts.
   1. Only backward compatible changes will be applied. Therefore it is not possible to remove items/components or even change the final status flag.
   1. Previous versions of NSIWS would respond with an error in such cases

### Details v7.11.2

1. SDMX-JSON structure bug fix related to stub categorisation from OECD
1. Include SDMX v2.0 Cross Sectional metadata in SDMX v2.1 XML and JSON output as annotations and concept roles
1. Allow SDMX v2.1 data requests for dataflow using SDMX v2.0 Cross Sectional DSD
1. Reference partial support for CategorySchemes from OECD
1. Support updating non-final artefacts that are referenced by other artefacts
1. Health check page show retriever version
1. OpenID middleware new configuration option
1. New log4net property to show logged user in logs

### Tickets v7.11.2

The following new features added:

- SDMXRI-1150: Include v2.0 CrossSectional information in SDMX-JSON (.net) (QTM2-2019.0281)
- SDMXRI-1151: SDMX v2.1 data/structure output for SDMX v2.0 XS DSD (.NET) (QTM2-2019.0281)
- SDMXRI-1117: Add user information to logging thread (OECD,QTM2-2019.0281)
- SDMXRI-1285: Tiny NSI WS improvements
- SDMXRI-1292: Implement partial CategorySchemes through detail=referencepartial parameter
- SDMXRI-1267: Not possible to update names/annotations in non-final artefacts that are referenced by other artefacts (OECD)

The following bugs have been corrected:

- SDMXRI-1286: Json writer bug writing categorisation with  allstubs detail (OECD, PR)

## nsiws.net v7.11.1 (2020-02-07) (MSDB v6.8) (AUTHDB v1.0)

### Details v7.11.1

1. Updated Dockerfile for .NET Core 3.1
1. New configuration file to set the maximum size of the request when using Kestrel (e.g. `dotnet NSIWebServiceCore`)
1. Fix issue related to `referencepartial`, concept schemes and DSD with components sharing the same concept identity
1. Merge fixes to JSON DSD parser from ISTAT

### Tickets v7.11.1

The following new features added:

- SDMXRI-1283: Update linux Dockerfile for .net core 3.1 (OECD)
- SDMXRI-1268: Configure max size of request message. (OECD)
- SDMXRI-1084: SDMX JSON DSD Parser (.NET) (ISTAT,QTM2-2018-SC000941)

The following bugs or regressions have been corrected:

- SDMXRI-1214: "Semantic Error - Duplicate language `it` for TextType" when using referencepartial (OECD)

## nsiws.net v7.11.0 (2020-01-20) (MSDB v6.8) (AUTHDB v1.0)

### Details v7.11.0

1. Uses [MSDB v6.8](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/msdb.sql/browse/HISTORY.md?at=refs%2Fheads%2F6.8)
1. Target .NET Core 3.1
1. Support for including NSIWS URL in the response of full artefacts requested as stubs
1. Support for including NSIWS URL in the response of full artefacts requested as stubs
1. Fix default header loading of configuration

### Tickets v7.11.0

The following new features added:

- SDMXRI-1244: NSI WS & MAWS target .NET Core 3.1
- SDMXRI-1140: Mechanism to set the NSI WS URL in returned stub artefacts (stored in full in MASTORE) (OECD,QTM2-2019.0281)
- SDMXRI-1268: Configure max size of request message.
- SDMXRI-1179: IIS workaround, remove WebDAVModule (QTM2-2019.0281)

The following bugs have been corrected:

- SDMXRI-1259: fix namespace for SettingsHeaderRetriever in the app.config file

#### MSDB v6.8 tickets

The following new features added:

- SDMXRI-1208: SDMX ID increase to 255 characters in MSDB (Oracle/MariaDB) (OECD,sync-needed)
- SDMXRI-1052: SDMX ID increase to 255 characters in MSDB (SQL) (OECD,QTM2-2018-SC000941)

The following bugs have been corrected:

- SDMXRI-1222: Referencepartial parameter for request with references doesn't work anymore
- SDMXRI-1123: Enhancements for command line MAAPI Tool (OECD,QTM2-2019.0281) (Partial only missing DROP statements were fixed)
- SDMXRI-1220: Cannot delete or replace categorisations and/or HCL

## nsiws.net v7.10.10 (2019-12-27) (MSDB v6.7) (AUTHDB v1.0)

### Details v7.10.10

1. Fix a security issue that affects MAWS.NET

### Tickets v7.10.10

The following bugs have been corrected:

- SDMXRI-1264: NSIWS/MAWS .NET thread principal not cleared

## nsiws.net v7.10.9 (2019-12-20) (MSDB v6.7) (AUTHDB v1.0)

### Details v7.10.9

1. OpenID improvements from OECD
1. Improve and correct references resolution (all, parents, children, parents and siblings and specific structures)

### Tickets v7.10.9

The following new features added:

- SDMXRI-1243: change OpenIdConnect anonymous access to be configurable
- SDMXRI-1194: Support for references parent/children for PA, Structure Set, MSD and MDF + Constraints (.NET) (ISTAT,OECD,QTM2-2019.0281)

## nsiws.net v7.10.8 (2019-12-06) (MSDB v6.7) (AUTHDB v1.0)

### Details v7.10.8

1. Allow admin user to access all mapping store, to avoid having the administrator to logout and login everytime there is a new Mapping Store.
   OAuth2 caches the user with the mapping store.
1. The keyword `latest` at version is not allowed when deleting artefacts
1. Fix SOAP 2.1 structure requests with CodeWhere and Stubs

### Tickets v7.10.8

The following bugs have been corrected:

- SDMXRI-1237: MAWS.NET cannot get SDMX artefacts after login with OAuth
- SDMXRI-1183: Deleting artefact using version of latest
- SDMXRI-1233: SOAP structure query doesn't return correct results on NSIWS.NET

## nsiws.net v7.10.6 (2019-11-19) (MSDB v6.7) (AUTHDB v1.0)

### Details v7.10.6

1. Another attempt to remove the Value element under TimeRange in Content Constraint
1. Replace isEqualVersion UDF with plain SQL
1. Fix packaging issue with missing Estat.Sri.Msdb.Sql.DLL

### Tickets v7.10.6

The following new features added:

- SDMXRI-1127: Replace UDF dbo.isEqualVersion by native SQL command (OECD)

The following bugs have been corrected:

- SDMXRI-899: Support for Content Constraint Reference Period and Time Range (.NET) (COLLAB,OECD,QTM2-2018-SC000941,SDMXTOOLSTF)

## nsiws.net v7.10.5 (2019-11-15) (MSDB v6.7) (AUTHDB v1.0)

### Details v7.10.5

1. Fix duplicate Vary header issue when it is enabled either in CORS and/or Server side caching.
   1. Now NSI WS Vary header added in SDMX REST calls is controlled by configuration. This does not affect Vary header added by CORS or Server side caching.
1. Fix SDMX v2.1 Content Constraint output issue, when there is a TimeRange inside a CubeRegion
1. Multidataset support in SDMX-JSON v1

### Tickets v7.10.5

The following new features added:

- SDMXRI-1218: Improve Vary header generation in NSIWS.NET (ILO,OECD)
- SDMXRI-1213: Invalid child element 'Value' in TimeRange of Actual Content Constraint
- SDMXRI-1091: SDMX-JSON data multi-dataset support (.NET)

The following bugs have been corrected:

- SDMXRI-1171: Re-submitting some existing artefact is reported as "Created"

## nsiws.net v7.10.3 (2019-10-29) (MSDB v6.7) (AUTHDB v1.0)

### Details v7.10.3

1. Open ID enhancements from OECD

### Tickets v7.10.3

The following new features added:

- SDMXRI-1207: Allow anonymous access with OpenIdConnect middleware, when no token in request + other minor fixes

## nsiws.net v7.10.2 (2019-10-25) (MSDB v6.7) (AUTHDB v1.0)

### Details v7.10.2

1. Improve performance when importing HCL
1. A lot of fixes to bugs and regressions related to stubs
   1. Could not categorise stub dataflows
   1. Structure/Service URL not retrieved from msdb
   1. Could not delete content constraint
1. Support parsing SDMX-JSON DataStructures (from ISTAT)

### Tickets v7.10.2

The following new features added:

- SDMXRI-1198: Improve performance when importing HCL (.net) (ISTAT,OECD)
- SDMXRI-1084: SDMX JSON DSD Parser (.NET) (ISTAT,QTM2-2018-SC000941)

The following bugs have been corrected:

- SDMXRI-1147: Cannot categorise Dataflows that are defined as external references
- SDMXRI-1137: Rest stub parameter results in isExternalReference="true" and `structureURL="http://need/to/changeit"`
- SDMXRI-1167: Not possible anymore to delete or update Allowed Content Constraint

## nsiws.net v7.10.1 (2019-10-11) (MSDB v6.7) (AUTHDB v1.0)

### Details v7.10.1

1. Allow to connect to OpenID service without SSL (OECD)
1. Bugfixes related to the VaryHeader not showing in Structure requests
1. Move all static files under wwwroot
1. Show URN parameter in case of a 406 Not accepted error
1. SDMX-JSON structure include external link in case for each artifact that is a stub/external (OECD)

### Tickets v7.10.1

The following new features added:

- SDMXRI-1164: Add configuration to authentication to disable SSL and token issuer (OECD)
- SDMXRI-1145: Add external link to a json structure message when artifact has isExternalReference=true (OECD)

The following bugs have been corrected:

- SDMXRI-1163: NSIWS Vary Header issue (ILO)
- SDMXRI-1138: nsiws static files (.NET) (OECD)
- SDMXRI-1165: Accept header parameter urn doesn't work (OECD)

## nsiws.net v7.10.0 (2019-09-27) (MSDB v6.7) (AUTHDB v1.0)

### Tickets v7.10.0

The following new features added:

- SDMXRI-1131: Create .net core healtcheck endpoint with json output (OECD)
- SDMXRI-720: Do not include URN in structural metadata output (COLLAB,OECD)

## nsiws.net v7.9.0 (2019-09-06) (MSDB v6.7) (AUTHDB v1.0)

### Details v7.9.0

1. Added OpenId [documentation](openid-middleware.md)
1. NSI WS will not output URN by default. To include URN then the `urn` parameter must be added in the `Accept` HTTP header

### Tickets v7.9.0

The following new features added:

- SDMXRI-720: Do not include URN in structural metadata output (COLLAB,OECD)
- SDMXRI-1094: Add Open-Id connect authentication pluggable middleware (OECD)

## nsiws.net v7.8.0 (2019-09-02) (MSDB v6.7) (AUTHDB v1.0)

### Details v7.8.0

1. Update 3rd party dependencies (from ILO)
1. Support for server side cachning from ILO
1. Further improvements in error messages when importing structual metadata

### Tickets v7.8.0

The following new features added:

- SDMXRI-1111: Server side caching from ILO for NSIWS.NET (ILO)
- SDMXRI-1108: SDMXRI.NET update DryIoc and Oracle dependencies (ILO)
- SDMXRI-854: NSI ws - enhanced status and error messages (.net)

## nsiws.net v7.7.0 (2019-08-23) (MSDB v6.7) (AUTHDB v1.0)

### Tickets v7.7.0

The following new features added:

- SDMXRI-899: Support for Content Constraint Reference Period and Time Range (.NET)
- SDMXRI-854: NSI ws - enhanced status and error messages (.net)
- SDMXRI-871: ContentConstraint retriever puts excluded key values as included
- SDMXRI-1013: SDMX REST support detail=allcompletestubs (.NET)
- SDMXRI-899: Support for Content Constraint Reference Period and Time Range (.NET)
- SDMXRI-861: Implement ''include history'' in SDMX REST (.net)
- SDMXRI-1095: Introduce AdoNetAppender to Log4Net to allow the configuration of the NSIWS to log into a database

## nsiws.net v7.6.0 (2019-07-22) (MSDB v6.7) (AUTHDB v1.0)

### Details v7.6.0

1. Uses [MSDB v6.7](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/msdb.sql/browse/HISTORY.md?at=refs%2Fheads%2F6.7)
1. Support for storing and retrieving SDMX stubs including the URL for structure or service
1. Support for updating the parent item of existing items and the parent item of new items
1. Support for Content Constraints that attach to:
   1. Dataset
   1. Metadataset
   1. Data Provider
   1. Simple datasource
   1. Complementing existing maintainable artefact attachment support,  queryable data source support
1. Support for Include History in REST data queries
1. Include a log4net plugin to allow logging on a database. Note no configuration is provided
   1. [Documentation can be found here](https://github.com/microknights/Log4NetAdoNetAppender)
1. Open ID authentication plugin (middleware) from OECD.

### Tickets v7.6.0

The following new features added:

- SDMXRI-1094: Add Open-Id connect authentication pluggable middleware (OECD)
- SDMXRI-861: Implement ''include history'' in SDMX REST (.net) (QTM6-2018.SC000641)
- SDMXRI-1098: SdmxSource produces ContentConstrain in 2.1 xml in some cases (.NET) (future-QTM)
- SDMXRI-1078: SDMX RI support Stubs (.NET) (ISTAT,OECD)
- SDMXRI-860: Content Constraint support attaching to Item and Data Sources (.net)
- SDMXRI-1021: NSI WS support updating the parent codes of existing final codelist(.net) (ISTAT,QTM2-2018-SC000941)
- SDMXRI-1095: Introduce AdoNetAppender to Log4Net to allow the configuration of the NSIWS to log into a database

## nsiws.net v7.5.0 (2019-06-25) (MSDB v6.6) (AUTHDB v1.0)

## Details v7.5.0

1. Fixes related to authentication via OAuth2 and .NET core migration issues
1. Improve error logging and exception handing (From OECD)

## Tickets v7.5.0

The following new features added:

- SDMXRI-1004: MAWEB login/authentication integrate with MAWS.NET (QTM2-2018-SC000941)

The following bugs have been corrected:

- SDMXRI-1066: Unhandled exception caught in JsonExceptionMiddleware is not logged and doesn't catch error from previous middlewares in the pipeline

## nsiws.net v7.4.0 (2019-05-24) (MSDB v6.5) (AUTHDB v1.0)

## Details v7.4.0

1. Uses MSDB v6.6. Code updated to work updated store procedures
1. Support for queyring content constraints by type in REST. In other words support the new SDMX REST resources /allowedconstraint and /actualconstraint.

## Tickets v7.4.0

The following new features added:

- SDMXRI-711: Get feature for (Valid and/or Actual) ContentContraints (.net) (COLLAB,OECD,QTM6-2018.SC000641)

## nsiws.net v7.3.1 (2019-04-30) (MSDB v6.5) (AUTHDB v1.0)

*WARNING* This version targets .NET Core 2.2 and require ASP.NET Hosting module to work with IIS

## Details v7.3.1

1. Documentation updates, configuration files is now `config/app.config`
1. Document CORS configuration

## Tickets v7.3.1

The following new features added:

- SDMXRI-849: Documentation for the CORS module in NSI WS (.net) (QTM6-2018.SC000641)
- SDMXRI-1015: Make NSI.WS work in Linux container (OECD)

## nsiws.net v7.3.0 (2019-04-25) (MSDB v6.5) (AUTHDB v1.0)

*WARNING* This version targets .NET Core 2.2 and require ASP.NET Hosting module to work with IIS

## Details v7.3.0

1. Support Content Constraints that attach to Metadataflows, Metadatastructure definitions and Provision Agreements
1. Support querying for item scheme items
1. Modify SdmxSource structure query classes to support item queries in both REST and SOAP v2.1.
1. SDMX-JSON hardcoded to use UTF-8 without the BOM. (Changes from OECD)

## Tickets v7.3.0

The following new features added:

- SDMXRI-713: Get references for specific items (in ItemSchemes) only .net (COLLAB,OECD,QTM6-2018.SC000641)
- SDMXRI-859: Support for content constraints attaching to other maintainable artefacts (.net) (QTM6-2018.SC000641)
- SDMXRI-1015: Make NSI.WS work in Linux container (OECD)

The following bugs have been corrected:

- SDMXRI-983: Use hardcoded UTF8 without BOM in json writers (OECD)

## nsiws.net v7.1.2 (2019-03-22) (MSDB v6.5) (AUTHDB v1.0)

*WARNING* This version targets .NET Core 2.2 and require ASP.NET Hosting module to work with IIS

## Details v7.1.2

1. Plugable Middleware, see [PLUGINS documentation](PLUGINS.md)
1. XML Authentication/Authorization store support. The authentication and authorization information is stored in a XML file.  The goal is to use it in tests. See [AUTH docuemntation](AUTH.md)
1. OECD SDMX Json format enhancements:
   1. Changes of data and structure message formats in JSON standard applied on JSON v1.0 components
   1. JSON data message. Set codelist code's parent ID in case of hierarchical codelist

## Tickets v7.1.2

The following new features added:

- SDMXRI-906: Change in localised text in SDMX-JSON data and structure messages
- SDMXRI-962: Updating big codelists
- SDMXRI-740: XML backend for new Authentication mechanism (QTM1-2018.SC000641)

Regressions related to the following tickets were fixed:

- SDMXRI-839: Migrate NSI WS and MAWS to .NET Core (QTM1-2018.SC000641)

## nsiws.net v7.1.1 (2019-03-08) (MSDB v6.5) (AUTHDB v1.0)

*WARNING* This version targets .NET Core 2.2 and require ASP.NET Hosting module to work with IIS

## Details v7.1.1

1. CORS bugfixes from OECD and fix regression with Expose headers
1. Policy module fixes
1. Include a XML based authentication/authorization module, disabled by default

## Tickets v7.1.1

The following new features added:

- SDMXRI-740: XML backend for new Authentication mechanism (QTM1-2018.SC000641)
- SDMXRI-935: Update CorsModule to work in .net core

The following bugs have been corrected:

- SDMXRI-957: NSI WS .NET regressions in policy module
- SDMXRI-839: (Regressions) Migrate NSI WS and MAWS to .NET Core (QTM1-2018.SC000641)
- SDMXRI-870: (Regression) CORS Support for Access-Control-Expose-Headers (.net)

## nsiws.net v7.1.0 (2019-02-15)

## Details v7.1.0

1. Support retrieving data and available constraint when frequencies with multipliers are used.
1. SDMX-JSON writer improvements from OECD
   1. Annotations added at all levels to json data & structure messages according to spec [https://github.com/sdmx-twg/sdmx-json/blob/master/data-message/docs/1-sdmx-json-field-guide.md](https://github.com/sdmx-twg/sdmx-json/blob/master/data-message/docs/1-sdmx-json-field-guide.md)
   1. Dataflow & DSD annotations are merged in json data message, if the annotation with the same type present then lower level takes priority.

## Tickets v7.1.0

The following new features added:

- SDMXRI-842: Support A10 frequency in SDMX RI (QTM1-2018.SC000641)
- SDMXRI-907: Add order property and annotations to�component value object in json writer.

## nsiws.net v7.0.0 (2019-02-07)  (MSDB v6.5) (AUTHDB v1.0)

*WARNING* This version targets .NET Core 2.2 and require ASP.NET Hosting module to work with IIS

## Details v7.0.0

1. Fix Provision agreement output
1. SDMX-CSV improvements from OECD, labels and timeFormat

### Breaking changes with v7.0.0

1. Migrate to .NET Core 2.2
1. ConnectionStrings are expected to be in `c:\ProgramData\Eurostat\nsiws.config`
1. IIS Configuration is still in `web.config`
1. other configuration are expected to be in `NSIWebServiceCore.config`
1. Default log file directory has changed to `c:\ProgramData\Eurostat\logs\`

## Tickets v7.0.0

The following new features added:

- SDMXRI-839: Migrate NSI WS and MAWS to .NET Core (QTM1-2018.SC000641)
- SDMXRI-903: Implement Accept header additional parameters (labels & timeFormat) to control CSV output (QTM6-2018.SC000641)
- SDMXRI-902: Apply timeFormat in csv writer to normalize date outputs (QTM6-2018.SC000641)
- SDMXRI-873: SdmxSource support HTTP 406 status code

The following bugs have been corrected:

- SDMXRI-872: Cannot query data with time period criteria without FREQ and TIME_PERIOD DB column an int on sqlserver
- SDMXRI-874: NSI WS .NET produces incorrect Provision Agreement structure

## nsiws.net v6.16.0 (2019-01-15) (MSDB v6.5) (AUTHDB v1.0)

This version targets .NET Framework 4.6.2

## Details v6.16.0

1. Switched to package references. As a result Visual Studio versions earlier than 2017 are no longer supported.
1. Renamed the nuget packages of Estat SdmxSource Extension, DataRetriever, StructureRetriever and SdmxSource
1. This is the first version that targets .NET Framework 4.6.2
1. JSON fixes from the latest SdmxSource
1. It is possible to configure NSI WS to cache results. See [Caching documentation](CACHING.md)
1. Disabled client side caching by default
1. Support for CORS `Access-Control-Expose-Headers` in the CORS module

## Tickets v6.16.0

The following new features added:

- SDMXRI-868: Rename nuget packages
- SDMXRI-838: SDMX RI libraries to target .NET Standard 2.0 (QTM1-2018.SC000641)
- SDMXRI-863: Output caching for rest GET data and structure services (OECD)
- SDMXRI-870: CORS Support for Access-Control-Expose-Headers

## nsiws.net v6.15.1 (2018-12-18)  (MSDB v6.5) (AUTHDB v1.0)

This version targets .NET Framework 4.5

## Details v6.15.1

1. Server-side caching implemented by OECD for SDMX REST requests. See [Documentation](CACHING.md)
1. Disable by default browser side caching.
1. Update dependencies

## Tickets v6.15.1

The following new features added:

- SDMXRI-863: Output caching for rest GET data and structure services (OECD)

## nsiws.net v6.15.0 (2018-10-31) (MSDB v6.5) (AUTHDB v1.0)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.5 (or partially v5.3)
If the provided authentication module is used, then minimum AUTH DB version v1.0

## Details v6.15.0

1. Allow adding new items to final item scheme. This is disabled by default. It can be enabled in configuration.
1. Fixes related to data availability, specifically an error occurred when a key was present.
1. Fix query time criteria handling on .NET when no time transcoding is used
1. Synchronize .NET SDMX v2.0 Dataset element ouput to Java
1. Fix Frequency code for reporting period
1. Support JSON Submit Structure
1. JSON fixes by OECD:
   1. Fix for the missing series level attributes when the component id is not identical to the concept id in json data messages
   1. Parent codes no longer presented at code lists with hierarchy in structure section of `data+json;version=1.0.0`

## Tickets v6.15.0

The following new features added:

- SDMXRI-824: Adding items into finalized item schemes
- SDMXRI-747: CORS support in NSI.WS (QTM1-2018.SC000641)
- SDMXRI-825: MAWS / MA_WEB integration (QTM-ma-frontend,QTM5-2018.SC000641)
- SDMXRI-721: Adjust REST data availability according the SDMX TWG proposal (COLLAB,OECD,QTM1-2018.SC000641)
- SDMXRI-835: Include the dataflowId and the dataflowAgencyId in Compact Data Dataset
- SDMXRI-712: SDMX-JSON structure submit via REST in NSI WS (COLLAB,OECD,QTM1-2018.SC000641)

The following bugs have been corrected:

- SDMXRI-739: No data returned when querying 2010 data from 2010-01-01 to 2010-12-31
- SDMXRI-760: Attribute management fix in csv,  json, xml (abstract, generic readers) (OECD)
- SDMXRI-667: SDMX v2.1 Time Range in .NET (QTM6-2017.SC000281)

## nsiws.net v6.14.0 (2018-09-26) (MSDB v6.5) (AUTHDB v1.0)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.5 (or partially v5.3)
If the provided authentication module is used, then minimum AUTH DB version v1.0

## Details v6.14.0

1. Updated to the latest SDMX RI dependencies
   1. DR v6.5.0
   1. SR v4.4.0
   1. MAAPI v1.13.0
   1. `SdmxSource` v1.12.1
1. Changes in the SDMX CSV writer error messages (OECD)
1. Align SDMX v2.1 Generic Data output to the Java version
1. Fix available constraint query handling
1. Fix text format issue in SDMX JSON Structure parser (OECD)
1. Fix time period output in SDMX-JSON data (OECD)
1. Fix SDMX-JSON DSD dimension position, it starts now from 0. (OECD)
1. Fix SDMX-JSON DSD concept role output, it now has the full URN instead of the id. (OECD)
1. More work towards data availability. Available mode is not yet implemented
1. Log the execution of SQL statements in a CSV file. Disabled by default. Can be enabled in log4net configuration.
1. Fix issue with Content Constraints importing

## Tickets v6.14.0

The following new features added:

- SDMXRI-589: Log duration of execution of SQL Queries  (COLLAB,QTM3-2017.SC000281,QTM6-2017.SC000281,QTM7-2017.SC000281,QTM8-2017.SC000281)
- SDMXRI-747: CORS support in NSI.WS (QTM1-2018.SC000641)
- SDMXRI-686: Generic 21 Format and `ObsDimension` TAG (QTM1-2018.SC000641)
- SDMXRI-642: SDMX-csv writer .NET (OECD)
- SDMXRI-756: Valid/actual content constraints not returned when requested as references to dataflow/dsd (QTM1-2018.SC000641)

The following bugs have been corrected:

- SDMXRI-783: WS doesn't include `SdmxSource` validation errors the response
- SDMXRI-778: NSI WS import Constraints with ReferencePeriod results to error
- SDMXRI-781: SDMX Structure JSON parser (V10) text format issue (OECD)
- SDMXRI-789: "id" and "name" missing for the TIME_PERIOD in JSON (3rd-level-support) (OECD)
- SDMXRI-669: Support for partial/special codelists in REST in .NET (QTM6-2017.SC000281)
- SDMXRI-785: SDMX JSON v1.0 structural metadata writer possibly issues (OECD)

## nsiws.net v6.13.1 (2018-08-07) (MSDB v6.5) (AUTHDB v1.0)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.5 (or partially v5.3)
If the provided authentication module is used, then minimum AUTH DB version v1.0

## Details v6.13.1

1. Bugfixes related to transcoding and the new formatVersion parameter
1. Handle gracefully missing configuration related to mapping
1. Fix Data Retriever version

## Tickets v6.13.1

The following bugs have been corrected:

- SDMXRI-772: NSI WS .NET Cannot retrieve data when Time Transcoding is used
- SDMXRI-673: `formatVersion` is optional

## nsiws.net v6.13.0 (2018-08-03) (MSDB v6.5) (AUTHDB v1.0)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.5 (or partially v5.3)
If the provided authentication module is used, then minimum AUTH DB version v1.0

## Details v6.13.0

1. NSI WS uses now MSDB v6.5. An upgrade is needed. See [Tool.md]
   1. The [Release notes of MSBD v6.5](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/msdb.sql/browse/HISTORY.md?at=6.5)
1. Several bugfixes/changes related Paging using HTTP Range
   1. *Important* it uses a 0 based index now. This means for example get the first 10 items/observations the Range header requires a value like `Range: values=0-9`
   1. Item schemes were not ordered which could lead to different results
   1. An additional item was returned
   1. Follow the `RFC 7233` closer
1. Support for several SDMX REST v1.2.0 features:
   1. If-Modified HTTP Header
   1. Vary HTTP Header
   1. `SDMX` error `Semantic error` now maps to HTTP status code `403 Forbidden`
   1. Format and version can be specified in the URL. Additional formats can be added in the `Web.Config` under `<FormatMapping>/<Mappings>`. e.g.

```xml
   <FormatMapping>
    <Mappings>
      <Mapping Format="genericdata" AcceptHeader="application/vnd.sdmx.genericdata+xml"/>
      <Mapping Format="jsondata" AcceptHeader="application/vnd.sdmx.data+json"/>
      <Mapping Format="structure" AcceptHeader="application/vnd.sdmx.structure+xml"/>
      <Mapping Format="structurespecificdata" AcceptHeader="application/vnd.sdmx.structurespecificdata+xml"/>
      <Mapping Format="csv" AcceptHeader="application/vnd.sdmx.data+csv"/>
    </Mappings>
  </FormatMapping>
```

1. Bugfixes from OECD in JSON, CSV and SDMX-ML formats.
1. Use default mapping store `MappingStoreServer` when Submitting structural metadata and no authentication or old authentication is used.
1. New features from OECD
   1. Support for retrieving content constraints as parents of DSDs and dataflows.
   1. Support for new SDMX-REST feature, [reference partial dependencies](https://github.com/sdmx-twg/sdmx-rest/blob/develop/v2_1/ws/rest/docs/4_3_structural_queries.md#parameters-used-to-further-describe-the-desired-results)

## Tickets v6.13.0

The following new features added:

- SDMXRI-673: SDMX RI  support for SDMX REST v1.2.0  (QTM6-2017.SC000281,QTM8-2017.SC000281)
- SDMXRI-675: POST & Update support in .NET (QTM6-2017.SC000281)
- SDMXRI-605: Extend SDMX RI permission roles (COLLAB,QTM6-2017.SC000281,QTM8-2017.SC000281)
- SDMXRI-596: Support for applying content constrains to codelists in REST (COLLAB,DevelopedByOECD,QTM6-2017.SC000281,QTM8-2017.SC000281)
- SDMXRI-760: Attribute management fix in csv,  json, xml (abstract, generic readers)
- SDMXRI-667: SDMX v2.1 Time Range in .NET (QTM6-2017.SC000281)
- SDMXRI-755: Partial codelists  review of .NET implementation (QTM1-2018.SC000641)
- SDMXRI-637: Split Mapping Store DDL scripts (QTM6-2017.SC000281)
- SDMXRI-605: Extend SDMX RI permission roles (COLLAB,QTM6-2017.SC000281,QTM8-2017.SC000281)

The following bugs have been corrected:

- SDMXRI-766: Range header returning 416 when it overlaps
- SDMXRI-601: SDMX REST paging (QTM6-2017.SC000281,QTM8-2017.SC000281)
- SDMXRI-751: SRI .NET 6.12.0
- SDMXRI-749: Problem with turkich language

## nsiws.net v6.12.1 (2018-07-11) (MSDB v6.4) (AUTHDB v1.0)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.4 (or partially v5.3)
If the provided authentication module is used, then minimum AUTH DB version v1.0

## Details v6.12.1

1. Bugfixes for logging and the blocking issue with data queries

## Tickets v6.12.1

The following new features added:

- SDMXRI-747: CORS support in NSI.WS (From OECD)

The following bugfixes added:

- SDMXRI-750: Test Client and the latest NSI WS
- SDMXRI-754: NSIWS .NET not logging (randomly)

## nsiws.net v6.12.0 (2018-07-02) (MSDB v6.4) (AUTHDB v1.0)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.4 (or partially v5.3)
If the provided authentication module is used, then minimum AUTH DB version v1.0

## Details v6.12.0

1. Bugfixes related to provision agreement retrieval, SDMX RI authentication and SDMX REST paging.
1. Data availability related changes. Only exact mode implemented. See [latest changes](https://github.com/sdmx-twg/sdmx-rest/blob/develop/v2_1/ws/rest/docs/4_6_1_other_queries.md).
   1. Available mode and data provider references are not supported.
1. SDMX JSON & CSV improvements from OECD
1. The `Content-Disposition` is included in the response when `;file` is added in the `Accept` HTTP Header.
1. When writing SDMX v2.0 datasets with DSD specific schema, use the same namespace as in Converter and Registry.
1. Allow plugging 3rd party DDB connection builders. The plugin to use is set in the `Web.Config` configuration option:

```xml
<add key="dbConnectionBuilderDll" value="Estat.Sri.Mapping.MappingStore"/>
```

## Tickets v6.12.0

The following new features added:

- SDMXRI-721: Adjust REST data availability according the SDMX TWG proposal
- SDMXRI-708: Change behavior of SDMX RR Append/Replace and REST POST/PUT
- SDMXRI-727: New SDMX-JSON data message format
- SDMXRI-702: SDMX JSON Structure Reader & Writer
- SDMXRI-707: Add possibility to return sdmx structure/data messages as file attachment in REST service
- SDMXRI-715: Allow using non-final codelists for attributes
- SDMXRI-693: SDMX RI vs SDMX Converter SDMX v2.0 data namespaces
- SDMXRI-590: Allow to plug DDB Connection builders

The following bugfixes added:

- SDMXRI-568: SDMX RI Authentication, outside Mapping Store
- SDMXRI-601: SDMX REST paging

## nsiws.net v6.11.0 (2018-05-01) (2018-03-15) (MSDB v6.4) (AUTHDB v1.0)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.4 (or partially v5.3)
If the provided authentication module is used, then minimum AUTH DB version v1.0

## Details v6.11.0

1. Revert to packages.config in the source code
1. _Draft_ support for data availability using the*temporary* resoruce `dynamiconstraints`. The resource will change as soon as it is decided. Also missing are the query parameters for the metric and mode.

  Example request:

```http
  GET http://localhost/ws/rest/dynamiconstraints/ESTAT,SSTSCONS_PROD_M,2.0/T..2../ALL/FREQ?reference=codelist HTTP/1.1
```

1. Support for SDMX v2.1 Time Range (not transcoding yet) and timezone offset.
1. SDMX REST paging for item schemes and data using HTTP Range header. Example [Test SOAP UI project](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxritests/browse/REST-Paging-soapui-project.xml)
1. It is possible to retrieve provision agreements
1. Fixed issue in ContentConstraint retrieval which ignored the field value of Actual
1. Plugin for SDMX-CSV that uses OECD SDMX CSV data writer.

## Tickets v6.11.0

The following new features added:

- SDMXRI-669: Support for partial/special codelists in REST in .NET (DRAFT)
- SDMXRI-708: Change behavior of SDMX RR Append/Replace and REST POST/PUT
- SDMXRI-596: Retrieve Provision Agreement and fix Actual flag in Content Constraints retrieval
- SDMXRI-678: SDMXRI-642 NSI WS to use the SDMX-CSV
- SDMXRI-601: SDMX REST paging
- SDMXRI-667: SDMX v2.1 Time Range in .NET (No SDMX RI transcoding support yet)

The following bugs have been corrected:

- SDMXRI-716: SDMX REST structure query parameter detail=referencestubs not supported

## NSI Web Service v6.10.0 (2018-03-15) (MSDB v6.4) (AUTHDB v1.0)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.4 (or partially v5.3)
If the provided authentication module is used, then minimum AUTH DB version v1.0

## Details v6.10.0

### New Authentication/Authorization mechanism

Starting from this version the NSI WS uses an new Authentication and Access Rules mechanism.
The schema can be found at [authdb](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/authdb.sql/browse?at=refs%2Fheads%2F1.0)

In addition the access rules/roles are now checked outside the NSI WS in a HTTP module, [Estat.Sri.Ws.PolicyModule](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/nsiws.net/browse/src/Estat.Sri.Ws.PolicyModule?at=refs%2Fheads%2F6.10.0)

For more information see [Authentication/Authorization documentation](AUTH.md)

### Reporting period

*Note 1* Reporting period is supported only in SDMX v2.1 standard.
*Note 2* special attribute `REPORTING_YEAR_START_DAY` is supported when attached to DataSet and Series level.

Reporting period and the special attribute `REPORTING_YEAR_START_DAY` are better supported.
It is possible now to output Reporting Period values such as `2001-M01` even when using Time Transcoding.

### Known issues v6.10.0

1. PC Axis as a DDB is not supported
1. DDB Plugins are needed instead of just configuration option

## Tickets v6.10.0

The following new features added:

- SDMXRI-568: SDMX RI Authentication, outside Mapping Store
- SDMXRI-537: Support transcoding for non-calendar years

The following bugs have been corrected:

- SDMXRI-692: When observation value is null, generate "null" in JSON

## NSI Web Service_v6.9.0 (2018-01-31) (MSDB v6.4)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.4 (or partially v5.3)

### Detailed description v6.9.0

This version depends on MSDB v6.4 which contains bug fixes and performance improvements.

For more information on what has changed on MSDB side please visit the [MSDB HISTORY](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/msdb.sql/browse/HISTORY.md?at=refs%2Fheads%2F6.4)

This release includes:

1. This release re-enables disabled features from v6.8.0
1. New configuration option and query parameter to include local codes in the output as annotations, see [Extended API](EXTENDED_API.md) and [Configuration](CONFIGURATION.md)
1. New data output format that contains a validation report see [Extended API](EXTENDED_API.md)
1. Merged improvement from OECD related to REST and having http and https endpoints.
1. Decrease size of structural metadata in SDMX-ML formats by having better namespace management.

*Note* Building Data Retriever requires Visual Studio 2017 Community or better or [Build Tools for Visual Studio 2017](https://www.visualstudio.com/downloads/#build-tools-for-visual-studio-2017)

### Known issues v6.9.0

1. PC Axis as a DDB is not supported
1. New Authentication system will be replaced
1. DDB Plugins are needed instead of just configuration option

### Tickets v6.9.0

The following new features added:

- SDMXRI-597 - MA WS/API Mapped data validation & errors
- SDMXRI-599 - MA WS/API get mapped data with local codes
- SDMXRI-536 - For WCF/Rest support of both http & https binding
- SDMXRI-593: Inefficient namespace management in generated SDMX structure messages

## NSI Web Service_v6.8.0 (2018-01-31) (MSDB v6.4)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.4 (or partially v5.3)
*NOTE* This release has experimental features from 6.7.0 disabled related to Submit Structural metadata.
For more information please see the full [6.8.0 release notes](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/nsiws.net/browse/CHANGELOG.md?at=refs%2Fheads%2F6.8.0)

### Detailed description v6.8.0

This version depends on MSDB v6.4 which contains bugfixes and performance improvements.

For more information on what has changed on MSDB side please visit the [MSDB HISTORY](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/msdb.sql/browse/HISTORY.md?at=refs%2Fheads%2F6.4)

This release includes:

1. .NET synchronized with Java version
1. The REST url for submitting structural metadata has changed to `/structure` from `/`
1. Use the same WSD target namespace for Submit Structure as in Java
1. Legacy authentication/authorization users, `TestAuthConfig`, are now able again to submit structural metadata.
1. REST requests no longer require a slash `/` at the end of the URL. In the past not ending the REST URL with a `/` would make `WCF` respond with a redirection request.
1. Corrected Cross Sectional dataset writing when there is no measure dimension and the primary measure concept reference id is not OBS_VALUE
1. A command line tool, DataMonitorJob.exe modified to work with non standard Global Registry REST API for submitting data registrations. See [Tool](Tool.md).

### Known issues v6.8.0

1. PC Axis as a DDB is not supported
1. New Authentication system will be replaced
1. DDB Plugins are needed instead of just configuration option

### Tickets v6.8.0

The following new features added:

- SDMXRI-648 - Sync .NET WS to JAVA REST POST URL
- SDMXRI-649 - .NET sync to Java. SOAP Registry Endpoint use Java namespace WSDL
- SDMXRI-650 - .NET Sync to Java TestAuthConfig users should be allowed to submit data
- SDMXRI-595 - Strange behaviour of redirection when not using trailing slash at end of Rest URLs

## NSI Web Service_v6.7.0 (2017-12-08) (MSDB v6.4)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.4 (or partially v5.3)

### Detailed description v6.7.0

This version depends on MSDB v6.4 which contains bugfixes and performance improvements.

For more information on what has changed on MSDB side please visit the [MSDB HISTORY](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/msdb.sql/browse/HISTORY.md?at=refs%2Fheads%2F6.4)

This release includes:

1. A new command line tool, `DataMonitorJob.exe`. When run it will check if there are data updates for each provision agreement and send data registrations to the configured SDMX Registry. See [Tool](Tool.md).
1. On the fly DSD support at Mapping Assistant API/WS. It is now possible to create a DSD/Dataflow and a Mapping Set from a DataSet.
1. Updating the non-final attributes names, descriptions and annotations of *final* Artefacts is now possible. See [Submitting Structural Metadata](SUBMIT_STRUCTURE.md)
1. *Important* New Plugin API for submitting structural metadata that tries to follow the Java API, see [Plugins](PLUGINS.md) for more information.
1. A new command line option `-f` to the Estat.Sri.Mapping.Tool.exe from OECD to force initialization/upgrade.
1. Better error reporting in case of appending, replacing or deleting structural metadata. See [Submitting Structural Metadata](SUBMIT_STRUCTURE.md)
1. Support for configuring a default value for Observation value, when there is no value in the database. Possible options are `NaN` or nothing.

### Known issues v6.7.0

1. PC Axis as a DDB is not supported
1. New Authentication system will be replaced
1. DDB Plugins are needed instead of just configuration option

### Tickets v6.7.0

The following new features added:

- SDMXRI-550: Allow updating of non-final properties of a final artefact
- SDMXRI-456: "Internal Server Error" message instead of the specific message returned previously
- SDMXRI-626: When OBS_VALUE is null in the DDB the output should contains NaN or nothing.

## NSI Web Service_v6.6.1 (2017-11-08)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.3 (or partially v5.3)

### Detailed description v6.6.1

This release includes bugfixes. In one case is documentation for the new configuration option to work with Mapping Store v5.3 and in the other the new RDF plugin was blocking all data SOAP requests.

### Known issues v6.6.1

1. PC Axis as a DDB is not supported
1. New Authentication system will be replaced
1. DDB Plugins are needed instead of just configuration option
1. Submitting structural metadata error reporting is minimal

### Tickets v6.6.1

The following bugs have been corrected:

- SDMXRI-583: Add missing documentation
- SDMXRI-621: .NET WS V6.6.0 error with soap endpoint with all SDMX format except Cross Sectional

## NSI Web Service_v6.6.0 (2017-10-31)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.3 (or partially v5.3)

### Detailed description v6.6.0

As a result of on going work on On The Fly DSD a new version of MSDB has been released and MAAPI depends on it.
*Note* On The Fly DSD support is not included in this release.

For more information on what has changed on MSDB side please visit the [MSDB HISTORY](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/msdb.sql/browse/HISTORY.md?at=refs%2Fheads%2F6.3)

1. A micro-data enhancement, the SDMX writers have been modified so it doesn't write a `NaN` when no observation value is given
1. Add an `Other` data type, so new data format plugins can use it
1. New RDF data and structure format plugin from ISTAT
1. Support for importing non-final structural metadata that depends on non-final structural  metadata. Please note support is limited to Dataflow to DataStructure, DataStructure to Codelist and Concept Scheme.
1. It is now possible to retrieve Mapping Sets, Mappings and Transcodings from MSDB v5.3. It needs the following configuration setting in order to be enabled:

```xml
<appSettings>
  <add key="MappingStoreVersion53" value="5.3"/>
```

### Known issues v6.6.0

1. PC Axis as a DDB is not supported
1. New Authentication system will be replaced
1. DDB Plugins are needed instead of just configuration option
1. Submitting structural metadata error reporting is minimal

### Tickets v6.6.0

The following new features added:

- SDMXRI-564: EGR Obs value NaN vs no value output
- SDMXRI-563: ISTAT RDF Writer
- SDMXRI-540: Allow to import a dsd in which there is a reference to a not final codelist
- SDMXRI-583: NSI WS should also support latest public release Mapping Store v5.3

## NSI Web Service_v6.5.1 (2017-10-18)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.2

### Detailed description v6.5.1

This is a bug fix release. It fixes issues related to Mapping Assistant DB id and IIS.

## NSI Web Service_v6.5.0 (2017-10-06)

Minimum Mapping Assistant DB (MADB/MSDB) version v6.2

### Detailed description v6.5.0

*WARNING* .NET 4.5 is now required by NSI WS
*WARNING* Authentication must be enabled to submit structural metadata, please refer to the installation guide, _Section 4.12 Registry Services � SubmitStructure_
*Note* Authentication/Authorization the Mapping Store database is used

Most changes are internal and affect mostly developers.

The following notes are for developers only:

- The plugin system will load *all* public implementations from a plugin dll. In case this creates issues, please change the visibility of the class. In practice the [DryIoC RegisterMany](https://bitbucket.org/dadhi/dryioc/wiki/RegisterResolve#markdown-header-registermany) is used with each assembly.
- SDMX RI DataRetriever and Structure Retriever use the MAAPI (`Estat.Sri.Mapping.Api`) to retrieve the mapping sec configuration from the Mapping Assistant DB
- The DLL names and NuGet packages for mapping store retrieval/store have been modified to reflect better their functionality
  - `MappingStoreRetrieval.NuGet` changed to `Estat.Sri.Sdmx.MappingStore.Retrieve`
  - `Estat.Sri.MappingStore.Store` changed to `Estat.Sri.Sdmx.MappingStore.Store`
  - In addition versions of the above packages have synchronized with MAAPI so the latest versions are 1.5.0
  - Namespaces remain the same but some classes/interface have moved to `Estat.SdmxSource.Extension.NuGet` (v1.6.1 or higher), `Estat.Sri.Mapping.Api` (v1.5.0 or higher) or `Estat.Sri.Mapping.Store` (v1.5.0 or higher)
- Changes inside the SRI Retriever plugin allow now:
  - To switch between different Mapping Assistant DB, but this is enabled only when used within MA WS
  - To inject a 3rd party method for generating a .NET `System.Data.DbConnection` from a MA API `DbbConnectionEntity` which contains the information from the table `DB_CONNECTION`.
- New plugins which are used by MA API.
  - `Estat.Sri.Mapping.MappingStore` it uses the Mapping Assistant DB.
  - `Estat.Sri.Plugin.MySql` a plugin for accessing databases on MariaDB/MySQL
  - `Estat.Sri.Plugin.Oracle` a plugin for accessing databases on Oracle
  - `Estat.Sri.Plugin.SqlServer` a plugin for accessing databases on SqlServer
  - `Estat.Sri.Plugin.Odbc` a plugin for accessing databases via ODBC (limited support/unsupported)

### Known issues v6.5.0

1. PC Axis as a DDB is not supported
1. New Authentication system will be replaced
1. DDB Plugins are needed instead of just configuration option
1. Submitting structural metadata error reporting is minimal

### Tickets v6.5.0

The following new features added:

- SDMXRI-536: OECD follow up meeting on some NSI WS related questions
- SDMXRI-552: Migrate DR and SR to use MA API
- SDMXRI-558: support multiple mapping stores (NSI & MA WS)

## NSI Web Service_v6.4.1 (2017-09-01)

The following new features have been added:

- Updated dependencies

## NSI Web Service_v6.4.0 (2017-08-05)

### Detailed description v6.4.0

*WARNING* .NET 4.5 is now required by NSI WS

Support for a default value for transcoding has been added. This is the value that will be used when there is no transcoding for a local value or when the value is null or empty.
Support for transcoding uncoded components.
For more information on what has changed on MSDB side please visit the [MSDB HISTORY](https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/msdb.sql/browse/HISTORY.md?at=refs%2Fheads%2F6.1)

### Tickets v6.4.0

The following new features added:

- SDMXRI-468: Support of transcoding/mapping of emtpy values
- SDMXRI-469: Transcoding for uncoded DSD components
- SDMXRI-114: Import of Categorisation that uses category from a non-final Category Scheme is now supported because it was allowed in Mapping Assistant and it blocked the import (with SDMX) feature

## NSI Web Service_v6.3.0 (2017-07-31)

### Detailed description 6.3.0

It is now possible to alter the maximum size of the received message via configuration

### Tickets 6.3.0

The following new features added:

- SDMXRI-507: NsiWs: Maximum length of the returned message

## NSI Web Service_v6.2.1 (2017-07-03)

The following bugs have been corrected:

- SDMXRI-530: Single quote in local code used in transcoding generates an error
- SDMXRI-466: MSD/MDF persist/retrieve

## NSI Web Service_v6.2.0 (2017-06-17)

The following new features added:

- SDMXRI-499: MA Web Service (Java/.NET) second release

## NSI Web Service_v6.1.0 (2017-05-22)

The following new features added:

- SDMXRI-444: Improve default header settings in NSI WS

## NSI Web Service_v6.0.1 (2017-04-26)

The following bugs have been corrected:

- SDMXRI-456: "Internal Server Error" message instead of the specific message returned previously

## NSI Web Service_v6.0.0 (2017-04-25)

The following new features added:

- SDMXRI-455: Submit via REST
- SDMXRI-466: MSD/MDF persist/retrieve
- SDMXRI-82: Maria BD / Linux / misc. enhancement
- SDMXRI-471: Implementation of Data Registration functionality
- SDMXRI-487: Modify ISTAT retriever code to be compatible future SDMX RI proof
- SDMXRI-465: ISTAT Review & Merge retrievers from ISTAT

The following bugs have been corrected:

- SDMXRI-258: GenericData flat dataset generation issue
