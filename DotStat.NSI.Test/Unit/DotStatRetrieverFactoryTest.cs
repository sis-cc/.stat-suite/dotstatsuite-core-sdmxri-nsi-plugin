﻿using System.Data.Common;
using System.Security.Principal;
using DotStat.NSI.RetrieverFactory.Factory;
using DotStat.Test;
using Estat.Sdmxsource.Extension.Manager;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Microsoft.AspNetCore.Http;
using Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Builder;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
using Org.Sdmxsource.Sdmx.Api.Model.Header;

namespace DotStat.NSI.Test.Unit
{
    [TestFixture]
    class DotStatRetrieverFactoryTest : SdmxUnitTestBase
    {
        private readonly DotStatRetrieverFactory _dotStatRetrieverFactory;
        private readonly Mock<IPrincipal> _principal = new Mock<IPrincipal>();
        private readonly Mock<IDataQuery> _dataQuery = new Mock<IDataQuery>();

        public DotStatRetrieverFactoryTest() : base()
        {
            Mock<IHttpContextAccessor> httpContextAccessor = new Mock<IHttpContextAccessor>();
            httpContextAccessor.Setup(x => x.HttpContext.Request.Headers["Accept"]).Returns("json");
            httpContextAccessor.Setup(x => x.HttpContext.Request.Headers["X-DotStat-Release"]).Returns("");
            Mock<IHeader> header = new Mock<IHeader>();
            Mock<IHeaderRetrievalManager> headerRetrievalManager = new Mock<IHeaderRetrievalManager>();
            headerRetrievalManager.Setup(x => x.Header).Returns(header.Object);
            Mock<IConnectionStringBuilderManager> connectionStringBuilderManager = new Mock<IConnectionStringBuilderManager>();
            Mock<IBuilder<DbConnection, DdbConnectionEntity>> dbBuilder = new Mock<IBuilder<DbConnection, DdbConnectionEntity>>();
            Mock<IComponentMappingManager> componentMappingManager = new Mock<IComponentMappingManager>();
            Mock<IComponentMappingValidationManager> componentMappingValidationManager = new Mock<IComponentMappingValidationManager>();
            Mock<IDataflowPrincipalManager> dataflowPrincipalManager = new Mock<IDataflowPrincipalManager>();
           
            _dotStatRetrieverFactory = new DotStatRetrieverFactory(
                httpContextAccessor.Object,
                headerRetrievalManager.Object,
                connectionStringBuilderManager.Object,
                dbBuilder.Object,
                componentMappingManager.Object,
                componentMappingValidationManager.Object,
                dataflowPrincipalManager.Object,
                this.GetConfiguration()
            );
           
        }

        [Test]
        public void GetDataRetrievalTest()
        {
            Mock<IPrincipal> principal = new Mock<IPrincipal>();

           var dataRetrieval = _dotStatRetrieverFactory.GetDataRetrieval(SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne),
                principal.Object);

            Assert.IsInstanceOf<ISdmxDataRetrievalWithWriter>(dataRetrieval);
        }
        
        [Test]
        public void GetAdvancedDataRetrievalTest()
        {
            Assert.Throws<SdmxNotImplementedException>(() => _dotStatRetrieverFactory.GetAdvancedDataRetrieval(_principal.Object));

        }

        [Test]
        public void GetDataRetrievalWithCrossTest()
        {
            Assert.Throws<SdmxNotImplementedException>(() => _dotStatRetrieverFactory.GetDataRetrievalWithCross(SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne), _principal.Object));

        }
    }
}
