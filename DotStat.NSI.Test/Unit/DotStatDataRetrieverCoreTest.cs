using System;
using System.Collections.Generic;
using System.Security.Principal;
using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Db.Manager;
using DotStat.Db.Repository;
using DotStat.Db.Util;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.NSI.DataRetriever;
using DotStat.Test;
using Estat.Nsi.DataRetriever;
using Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
using Org.Sdmxsource.Sdmx.Api.Model.Header;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;

namespace DotStat.NSI.Test.Unit
{
    [TestFixture]
    public class DotStatDataRetrieverCoreTest : SdmxUnitTestBase
    {
        private readonly DotStatDataRetrieverCore _dotStatDataRetrieverCoreJson;
        private readonly DotStatDataRetrieverCore _dotStatDataRetrieverCore;
        private readonly Mock<IPrincipal> _principal = new Mock<IPrincipal>();
        private readonly Mock<IHeader> _header = new Mock<IHeader>();
        private readonly Mock<ISdmxDataRetrievalWithWriter> _sdmxDataRetrievalWithWriter = new Mock<ISdmxDataRetrievalWithWriter>();
        private readonly Mock<IDataQuery> _dataQuery = new Mock<IDataQuery>();
        private readonly Mock<IDbManager> _dbManagerMock = new Mock<IDbManager>();
        private readonly Mock<IManagementRepository> _managementRepository = new Mock<IManagementRepository>();
        private readonly Mock<IDataWriterEngine> _dataWriterEngine = new Mock<IDataWriterEngine>();
        private readonly Mock<IMappingStoreDataAccess> _mappingStoreDataAccess = new Mock<IMappingStoreDataAccess>();
        private readonly Mock<IObservationRepository> _observationRepository = new Mock<IObservationRepository>();
        private readonly Mock<IAttributeRepository> _attributeRepository = new Mock<IAttributeRepository>();
        private readonly Mock<IAuthorizationManagement> _authManagement = new Mock<IAuthorizationManagement>();
        private readonly Dataflow _dataflow = null;
        private IAuthConfiguration _authConfiguration;
        
        private string _dataSpace;
        private TargetVersion _targetVersion;

        public DotStatDataRetrieverCoreTest() : base("sdmx/ATTRIBUTE_TEST_1.xml")
        {
            _dataSpace = "dummySpace";
            _targetVersion = TargetVersion.Live;
            _dataflow  = base.GetDataflow();

            _mappingStoreDataAccess.Setup(x => x.GetDataflow(
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<bool>()
                ))
                .Returns(_dataflow);

            _dbManagerMock
                .Setup(x => x.GetManagementRepository(_dataSpace))
                .Returns(_managementRepository.Object);

            _observationRepository.Setup(x => x.GetObservations(
                    _dataQuery.Object,
                    _dataflow,
                    It.IsAny<ICodeTranslator>(),
                    _dataSpace,
                    _targetVersion, 
                    1, 
                    11
             ))
             .Returns(ObservationGenerator.Generate(_dataflow, 2001, 2002, 2000));

            _dataQuery.Setup<IDataflowObject>(x => x.Dataflow).Returns(_dataflow.Base);
            _dataQuery.Setup<IDataStructureObject>(x => x.DataStructure).Returns(_dataflow.Dsd.Base);
            _dataQuery.Setup<string>(x => x.DimensionAtObservation).Returns("OBS_VALUE");

            _authConfiguration = new AuthConfiguration()
            {
                ClaimsMapping = new Dictionary<string, string>(),
                Enabled = false
            };

            _sdmxDataRetrievalWithWriter.Setup(x => x.GetData(_dataQuery.Object, _dataWriterEngine.Object));

            _dotStatDataRetrieverCoreJson = new DotStatDataRetrieverCore(
                _principal.Object,
                _header.Object,
                _sdmxDataRetrievalWithWriter.Object,
                _dbManagerMock.Object,
                _mappingStoreDataAccess.Object,
                _observationRepository.Object,
                _attributeRepository.Object,
                _authManagement.Object,
                _authConfiguration,
                new Tuple<long?, long?>(0, 10),
                "json",
                _targetVersion,
                _dataSpace);

            _dotStatDataRetrieverCore = new DotStatDataRetrieverCore(
                _principal.Object,
                _header.Object,
                _sdmxDataRetrievalWithWriter.Object,
                _dbManagerMock.Object,
                _mappingStoreDataAccess.Object,
                _observationRepository.Object,
                _attributeRepository.Object,
                _authManagement.Object,
                _authConfiguration,
                new Tuple<long?, long?>(0, 10),
                "dummy",
                _targetVersion,
                _dataSpace);
        }

        [TearDown]
        public void TearDown()
        {
            _authManagement.Invocations.Clear();
        }

        [Test]
        public void GetDataV8DataRetrieverTest()
        {
            _dataWriterEngine.Reset();

            _mappingStoreDataAccess.Setup(mock => mock.HasMappingSet(It.IsAny<IDataflowObject>())).Returns(false);

            _attributeRepository.Setup(x => x.GetKeyables(
                    _dataQuery.Object,
                    _dataflow,
                    It.IsAny<ICodeTranslator>(),
                    _dataSpace,
                    _targetVersion
                ))
                .Returns(
                    new List<IKeyable> {
                        new KeyableImpl(_dataflow.Base, _dataflow.Dsd.Base, new List<IKeyValue> {  }, new List<IKeyValue> { }, "dummyGroup", TimeFormat.Date, null )
                    });

            _dotStatDataRetrieverCore.GetData(_dataQuery.Object, _dataWriterEngine.Object);

            _dataWriterEngine.Verify(mock => mock.StartDataset(It.IsAny<IDataflowObject>(), It.IsAny<IDataStructureObject>(), It.IsAny<IDatasetHeader>()), Times.Once);
            _dataWriterEngine.Verify(mock => mock.StartGroup("dummyGroup"), Times.Once);
            _dataWriterEngine.Verify(mock => mock.StartSeries(), Times.AtLeastOnce);
            _dataWriterEngine.Verify(mock => mock.WriteSeriesKeyValue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            _dataWriterEngine.Verify(mock => mock.WriteAttributeValue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            _dataWriterEngine.Verify(mock => mock.WriteHeader(It.IsAny<IHeader>()), Times.Once);
        }

        [TestCase(true, true)]
        [TestCase(true, false)]
        [TestCase(false, true)]
        public void AuthTest(bool isAuthEnabled, bool isAuthorized)
        {
            _authConfiguration.Enabled = isAuthEnabled;
            _authManagement.Setup(x => x.IsAuthorized(
                    It.IsAny<DotStatPrincipal>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<PermissionType>())
                )
                .Returns(isAuthorized);

            if (isAuthEnabled && !isAuthorized)
            {
                Assert.Throws<DataRetrieverException>(() => _dotStatDataRetrieverCore.GetData(_dataQuery.Object, _dataWriterEngine.Object));
            }
            else
            {
                _dotStatDataRetrieverCore.GetData(_dataQuery.Object, _dataWriterEngine.Object);
            }

            _authManagement.Verify(mock => mock.IsAuthorized(
                    It.IsAny<DotStatPrincipal>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<PermissionType>()
                ),
                isAuthEnabled ? Times.Once() : Times.Never()
            );
        }

        [Test]
        public void GetDataV8DataRetrieverJsonTest()
        {
            _mappingStoreDataAccess.Setup(mock => mock.HasMappingSet(It.IsAny<IDataflowObject>())).Returns(false);

            //Setup for writing dataset level attributes
            _attributeRepository.Setup(mock => mock.GetDatasetAttributes(
                    _dataQuery.Object,
                    _dataflow,
                    It.IsAny<ICodeTranslator>(),
                    _dataSpace,
                    _targetVersion
            ))
            .Returns(new List<IKeyValue> { new KeyValueImpl("A", "dummyDim") });

            //Setup for writing attributes
            _attributeRepository.Setup(mock => mock.GetKeyables(
                    _dataQuery.Object,
                    _dataflow,
                    It.IsAny<ICodeTranslator>(),
                    _dataSpace,
                    _targetVersion)
             )
             .Returns(
                new List<IKeyable> {
                    new KeyableImpl(_dataflow.Base, _dataflow.Dsd.Base, null, new List<IKeyValue> { new KeyValueImpl("A", "FREQ") })
                });

            _dotStatDataRetrieverCoreJson.GetData(_dataQuery.Object, _dataWriterEngine.Object);

            _dataWriterEngine.Verify(mock => mock.WriteAttributeValue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            _dataWriterEngine.Verify(x => x.WriteHeader(It.IsAny<IHeader>()), Times.AtLeastOnce);
            _dataWriterEngine.Verify(x => x.StartDataset(It.IsAny<IDataflowObject>(), It.IsAny<IDataStructureObject>(), It.IsAny<IDatasetHeader>()), Times.AtLeastOnce);

        }

        [Test]
        public void GetDataV8DataRetrieverSpecialTreatmentGroupAttributeTest()
        {
            _mappingStoreDataAccess.Setup(x => x.HasMappingSet(It.IsAny<IDataflowObject>())).Returns(false);

            //Setup for testing specialTreatmentForGroupAttributes
            _attributeRepository.Setup(mock => mock.GetKeyables(
                    _dataQuery.Object,
                    _dataflow,
                    It.IsAny<ICodeTranslator>(),
                    _dataSpace,
                    _targetVersion
                ))
                .Returns(
                    new List<IKeyable> {
                        new KeyableImpl(_dataflow.Base, _dataflow.Dsd.Base, new List<IKeyValue> {  }, new List<IKeyValue> { }, "dummyGroup", TimeFormat.Date, null )
                    });

            _dotStatDataRetrieverCoreJson.GetData(_dataQuery.Object, _dataWriterEngine.Object);

            //TODO: assert/verify 

        }

        [Test]
        public void GetDataDefaultDataRetriever()
        {
            _mappingStoreDataAccess.Setup(mock => mock.HasMappingSet(It.IsAny<IDataflowObject>())).Returns(true);
            _dotStatDataRetrieverCore.GetData(_dataQuery.Object, _dataWriterEngine.Object);

            _sdmxDataRetrievalWithWriter.Verify(mock => mock.GetData(_dataQuery.Object, _dataWriterEngine.Object), Times.Once);

        }

        [Test]
        public void GetNumberOfObservationsTest()
        {
            var expectedNumOfObservations = 10;

            _observationRepository.Setup(mock => mock.GetObservationsCount(
                _dataQuery.Object,
                _dataflow,
                It.IsAny<ICodeTranslator>(),
                _dataSpace,
                _targetVersion
            ))
            .Returns(expectedNumOfObservations);

            var noOfObs = _dotStatDataRetrieverCore.GetNumberOfObservations(_dataQuery.Object);

            Assert.AreEqual(noOfObs, expectedNumOfObservations);
        }
    }
}