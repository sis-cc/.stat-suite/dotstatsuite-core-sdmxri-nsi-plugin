/******************
	This test provies scenario for stress testing a NSI-WS:
		1.- Determine how your system will behave under extreme conditions.
		2.- Determine what is the maximum capacity of your system in terms of users or throughput.
		3.- Determine the breaking point of your system and its failure mode.
		4.- Determine if your system will recover without manual intervention after the stress test is over.
	
*******************/
import http from 'k6/http';
import { check, sleep, group, fail } from 'k6';

	
//Default NSI-WS host url
let NSIWS_HOSTNAME = "http://127.0.0.1:81";
if(typeof __ENV.NSIWS_HOSTNAME !== 'undefined'){
	NSIWS_HOSTNAME = __ENV.NSIWS_HOSTNAME;
}
		
let TEST_NAME = "Stress-test";
if(typeof __ENV.TEST_NAME !== 'undefined'){
	TEST_NAME = __ENV.TEST_NAME;
}

let INPUT_FILE = "./Resources/test-cases.json";
if(typeof __ENV.TEST_CASES_FILE !== 'undefined'){
	INPUT_FILE = __ENV.TEST_CASES_FILE;
}

//Load test cases from json file
const TEST_CASES= JSON.parse(open(INPUT_FILE));
	
//NSI-WS available data response types
const SUPPORTED_RESPONSE_FORMATS =["csv", "json", "xml"];
if(typeof __ENV.RESPONSE_FORMAT !== "undefined"){
	if(SUPPORTED_RESPONSE_FORMATS.indexOf(__ENV.RESPONSE_FORMAT) < 0) 
		fail(`The response format '${__ENV.RESPONSE_FORMAT}', is not supported.`);
}
	
export let options = {
	ext: {
		loadimpact: {
		  projectID: 3497345, //k6 CLOUD project id 
		  name: TEST_NAME
		}
	},
	//Target = number of max users to scale to
	stages: [
		{ duration: '2m', target: 10 }, // below normal load
		{ duration: '5m', target: 10 },
		{ duration: '2m', target: 40 }, // normal load
		{ duration: '5m', target: 40 },
		{ duration: '2m', target: 70 }, // around the breaking point
		{ duration: '5m', target: 70 },
		{ duration: '2m', target: 100 }, // beyond the breaking point
		{ duration: '5m', target: 100 },
		{ duration: '10m', target: 0 }, // scale down. Recovery stage.*/
	],
	thresholds: {
		'checks': ['rate>0.90'], // more than 90% success rate
		
        "http_req_duration{group:::Query type structure::Struc type agencyscheme}": ["p(95)<1000"],
        "http_req_duration{group:::Query type structure::Struc type categoryscheme}": ["p(95)<1000"],
        "http_req_duration{group:::Query type structure::Struc type categorisation}": ["p(95)<1000"],
        "http_req_duration{group:::Query type structure::Struc type codelist}": ["p(95)<1000"],
        "http_req_duration{group:::Query type structure::Struc type conceptscheme}": ["p(95)<1000"],
        "http_req_duration{group:::Query type structure::Struc type contentconstraint}": ["p(95)<1000"],
        "http_req_duration{group:::Query type structure::Struc type dataflow}": ["p(95)<1000"],
        "http_req_duration{group:::Query type structure::Struc type datastructure}": ["p(95)<1000"],		
        "http_req_duration{group:::Query type structure::Struc type hierarchicalcodelist}": ["p(95)<1000"],		
        "http_req_duration{group:::Query type structure::Struc type metadataflow}": ["p(95)<1000"],	
        "http_req_duration{group:::Query type structure::Struc type metadatastructure}": ["p(95)<1000"],	
        "http_req_duration{group:::Query type structure::Struc type structureset}": ["p(95)<1000"],	
				
        "http_req_duration{group:::Query type data::Format xml::Size extraSmall}": ["p(95)<8000"],
        "http_req_duration{group:::Query type data::Format xml::Size small}": ["p(95)<8000"],
        "http_req_duration{group:::Query type data::Format xml::Size medium}": ["p(95)<8000"],
        "http_req_duration{group:::Query type data::Format xml::Size large}": ["p(95)<15000"],
        "http_req_duration{group:::Query type data::Format xml::Size extraLarge}": ["p(95)<100000"],
		
        "http_req_duration{group:::Query type data::Format json::Size extraSmall}": ["p(95)<8000"],
        "http_req_duration{group:::Query type data::Format json::Size small}": ["p(95)<8000"],
        "http_req_duration{group:::Query type data::Format json::Size medium}": ["p(95)<8000"],
        "http_req_duration{group:::Query type data::Format json::Size large}": ["p(95)<10000"],
        "http_req_duration{group:::Query type data::Format json::Size extraLarge}": ["p(95)<100000"],
		
        "http_req_duration{group:::Query type data::Format csv::Size extraSmall}": ["p(95)<15000"],
        "http_req_duration{group:::Query type data::Format csv::Size small}": ["p(95)<8000"],
        "http_req_duration{group:::Query type data::Format csv::Size medium}": ["p(95)<8000"],
        "http_req_duration{group:::Query type data::Format csv::Size large}": ["p(95)<10000"],
        "http_req_duration{group:::Query type data::Format csv::Size extraLarge}": ["p(95)<100000"],
		
        "http_req_duration{datasetSize:extraSmall}": ["p(95)<15000"],
        "http_req_duration{datasetSize:small}": ["p(95)<15000"],
        "http_req_duration{datasetSize:medium}": ["p(95)<15000"],
        "http_req_duration{datasetSize:large}": ["p(95)<15000"],
        "http_req_duration{datasetSize:extraLarge}": ["p(95)<15000"],		
	},	
	//Discard the response bodies to lessen the amount of memmory required by the testing machine.
	discardResponseBodies: true,

};

export function setup() {
	// 2. setup code

	//check that the nsi-ws hostname is available
	let healhCheck = http.get(`${NSIWS_HOSTNAME}/health`);
	if (healhCheck.status !== 200){
		fail(`Error: the NSI-WS {${NSIWS_HOSTNAME}/health} is not responding.`);
	}
	
	console.log(`Testing the NSI-WS {${NSIWS_HOSTNAME}}`);
}

export default function() {
	//Generate random query 
	let testQuery = TEST_CASES[Math.floor(Math.random() * TEST_CASES.length)];
    //console.log('Query: ', testQuery.query);
	
	let params = { headers: {'Accept-Ecoding': 'gzip, deflate'}	};
		
	group(`Query type ${testQuery.queryType}`, function (){
			
		//structure query			
		if(testQuery.queryType==="structure"){
			group(`Struc type ${testQuery.structureType}`, function (){
				let response = http.get(
					`${NSIWS_HOSTNAME}${testQuery.query}`, //URL
					params, //headers 
				);	
								
				check(response, {
					"status is 200": (r) => r.status == 200
				});
			});
		}
		//data query
		else{
			//Generate random format 
			let responseFormat = SUPPORTED_RESPONSE_FORMATS[Math.floor(Math.random() * SUPPORTED_RESPONSE_FORMATS.length)];
			if(typeof __ENV.RESPONSE_FORMAT !== 'undefined')
				responseFormat = __ENV.RESPONSE_FORMAT;
			//console.log('Format: ', responseFormat);
			
			params.headers.Accept=`application/vnd.sdmx.data+${responseFormat}`;
	
			if(responseFormat==="xml")
				params.headers.Accept=`application/vnd.sdmx.genericdata+${responseFormat}`;
	
			group(`Format ${responseFormat}`, function (){
				group(`Size ${testQuery.responseSize}`, function (){
					let response = http.get(
						`${NSIWS_HOSTNAME}${testQuery.query}`, //URL
						{tags:{datasetSize:testQuery.datasetSize}},//tags
						params, //headers 
					);	
				
					check(response, {
						"status is 200": (r) => r.status == 200
					});
				});
			});
		}
	});
	//wait 1 second after each test iteration
	sleep(1);
}
