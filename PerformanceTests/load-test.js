/******************
	This test provies scenario for load testing a NSI-WS:
		1.- Assess the current performance of the NSI-WS under typical and peak load.
		2.- Make sure that the NSI-WS is continuously meeting the performance standards as changes are made to the system (code and infrastructure).
	
*******************/
import http from 'k6/http';
import { check, sleep, group, fail } from 'k6';

//Default NSI-WS host url
let NSIWS_HOSTNAME = "http://127.0.0.1:81";
if(typeof __ENV.NSIWS_HOSTNAME !== 'undefined'){
	NSIWS_HOSTNAME = __ENV.NSIWS_HOSTNAME;
}
		
let TEST_NAME = "Load-test";
if(typeof __ENV.TEST_NAME !== 'undefined'){
	TEST_NAME = __ENV.TEST_NAME;
}

//NSI-WS available data response types
const SUPPORTED_RESPONSE_FORMATS =["csv", "json", "xml"];
if(typeof __ENV.RESPONSE_FORMAT !== "undefined"){
	if(SUPPORTED_RESPONSE_FORMATS.indexOf(__ENV.RESPONSE_FORMAT) < 0) 
		fail(`The response format '${__ENV.RESPONSE_FORMAT}', is not supported.`);
}

let INPUT_FILE = "./Resources/test-cases.json";
if(typeof __ENV.TEST_CASES_FILE !== 'undefined'){
	INPUT_FILE = __ENV.TEST_CASES_FILE;
}

//Load test cases from json file
const TEST_CASES= JSON.parse(open(INPUT_FILE));
	
export let options = {
	ext: {
		loadimpact: {
		  projectID: 3497345, //k6 CLOUD project id 
		  name: TEST_NAME
		}
	},
	//Target = number of max users to scale to
	stages: [
		{ duration: '5m', target: 40 }, // simulate ramp-up of traffic from 1 to 40 users over 5 minutes.
		{ duration: '10m', target: 40 }, // stay at 40 users for 10 minutes
		{ duration: '3m', target: 100 }, // ramp-up to 100 users over 3 minutes (peak hour starts)
		{ duration: '2m', target: 100 }, // stay at 100 users for short amount of time (peak hour)
		{ duration: '3m', target: 40 }, // ramp-down to 40 users over 3 minutes (peak hour ends)
		{ duration: '10m', target: 40 }, // continue at 40 for additional 10 minutes
		{ duration: '5m', target: 0 }, // ramp-down to 0 users
	],
	thresholds: {
		'checks': ['rate>0.95'], // more than 95% success rate
		
        "http_req_duration{group:::Query type structure::Struc type agencyscheme}": ["p(95)<1000"],
        "http_req_duration{group:::Query type structure::Struc type categoryscheme}": ["p(95)<1000"],
        "http_req_duration{group:::Query type structure::Struc type categorisation}": ["p(95)<1000"],
        "http_req_duration{group:::Query type structure::Struc type codelist}": ["p(95)<1000"],
        "http_req_duration{group:::Query type structure::Struc type conceptscheme}": ["p(95)<1000"],
        "http_req_duration{group:::Query type structure::Struc type contentconstraint}": ["p(95)<1000"],
        "http_req_duration{group:::Query type structure::Struc type dataflow}": ["p(95)<1000"],
        "http_req_duration{group:::Query type structure::Struc type datastructure}": ["p(95)<1000"],		
        "http_req_duration{group:::Query type structure::Struc type hierarchicalcodelist}": ["p(95)<1000"],		
        "http_req_duration{group:::Query type structure::Struc type metadataflow}": ["p(95)<1000"],	
        "http_req_duration{group:::Query type structure::Struc type metadatastructure}": ["p(95)<1000"],	
        "http_req_duration{group:::Query type structure::Struc type structureset}": ["p(95)<1000"],	
				
        "http_req_duration{group:::Query type data::Format xml::Size extraSmall}": ["p(95)<8000"],
        "http_req_duration{group:::Query type data::Format xml::Size small}": ["p(95)<8000"],
        "http_req_duration{group:::Query type data::Format xml::Size medium}": ["p(95)<8000"],
        "http_req_duration{group:::Query type data::Format xml::Size large}": ["p(95)<15000"],
        "http_req_duration{group:::Query type data::Format xml::Size extraLarge}": ["p(95)<100000"],
		
        "http_req_duration{group:::Query type data::Format json::Size extraSmall}": ["p(95)<8000"],
        "http_req_duration{group:::Query type data::Format json::Size small}": ["p(95)<8000"],
        "http_req_duration{group:::Query type data::Format json::Size medium}": ["p(95)<8000"],
        "http_req_duration{group:::Query type data::Format json::Size large}": ["p(95)<10000"],
        "http_req_duration{group:::Query type data::Format json::Size extraLarge}": ["p(95)<100000"],
		
        "http_req_duration{group:::Query type data::Format csv::Size extraSmall}": ["p(95)<15000"],
        "http_req_duration{group:::Query type data::Format csv::Size small}": ["p(95)<8000"],
        "http_req_duration{group:::Query type data::Format csv::Size medium}": ["p(95)<8000"],
        "http_req_duration{group:::Query type data::Format csv::Size large}": ["p(95)<10000"],
        "http_req_duration{group:::Query type data::Format csv::Size extraLarge}": ["p(95)<100000"],
		
        "http_req_duration{datasetSize:extraSmall}": ["p(95)<100000"],
        "http_req_duration{datasetSize:small}": ["p(95)<100000"],
        "http_req_duration{datasetSize:medium}": ["p(95)<100000"],
        "http_req_duration{datasetSize:large}": ["p(95)<100000"],
        "http_req_duration{datasetSize:extraLarge}": ["p(95)<100000"],	
	},	
	//Discard the response bodies to lessen the amount of memmory required by the testing machine.
	discardResponseBodies: true,

};

export function setup() {
	// 2. setup code

	//check that the nsi-ws hostname is available
	let healhCheck = http.get(`${NSIWS_HOSTNAME}/health`);
	if (healhCheck.status !== 200){
		fail(`Error: the NSI-WS {${NSIWS_HOSTNAME}/health} is not responding.`);
	}
	
	console.log(`Testing the NSI-WS {${NSIWS_HOSTNAME}}`);
}

export default function() {
	//Generate random query 
	let testQuery = TEST_CASES[Math.floor(Math.random() * TEST_CASES.length)];
    //console.log('Query: ', testQuery.query);
	
	let params = { headers: {'Accept-Ecoding': 'gzip, deflate'}	};
		
	group(`Query type ${testQuery.queryType}`, function (){
			
		//structure query			
		if(testQuery.queryType==="structure"){
			group(`Struc type ${testQuery.structureType}`, function (){
				let response = http.get(
					`${NSIWS_HOSTNAME}${testQuery.query}`, //URL
					params, //headers 
				);	
								
				check(response, {
					"status is 200": (r) => r.status == 200
				});
			});
		}
		//data query
		else{
			//Generate random format 
			let responseFormat = SUPPORTED_RESPONSE_FORMATS[Math.floor(Math.random() * SUPPORTED_RESPONSE_FORMATS.length)];
			if(typeof __ENV.RESPONSE_FORMAT !== 'undefined')
				responseFormat = __ENV.RESPONSE_FORMAT;
			//console.log('Format: ', responseFormat);
			
			params.headers.Accept=`application/vnd.sdmx.data+${responseFormat}`;
	
			if(responseFormat==="xml")
				params.headers.Accept=`application/vnd.sdmx.genericdata+${responseFormat}`;
	
			group(`Format ${responseFormat}`, function (){
				group(`Size ${testQuery.responseSize}`, function (){
					let response = http.get(
						`${NSIWS_HOSTNAME}${testQuery.query}`, //URL
						{tags:{datasetSize:testQuery.datasetSize}},//tags
						params, //headers 
					);	
				
					check(response, {
						"status is 200": (r) => r.status == 200
					});
				});
			});
		}
	});
	//wait 1 second after each test iteration
	sleep(1);
}
