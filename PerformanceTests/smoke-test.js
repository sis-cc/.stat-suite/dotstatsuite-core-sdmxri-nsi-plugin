/******************
	This test provies scenario for smoke testing a NSI-WS:
		1.- smoke test provides a sanity check every time there are new changes to the NSI-WS.
		2.- Verify that your system doesn't throw any errors when under minimal load.
	
*******************/
import http from 'k6/http';
import { check, sleep, group, fail } from 'k6';

//Default NSI-WS host url
let NSIWS_HOSTNAME = "http://127.0.0.1:81";
if(typeof __ENV.NSIWS_HOSTNAME !== 'undefined'){
	NSIWS_HOSTNAME = __ENV.NSIWS_HOSTNAME;
}
	
let TEST_NAME = "Smoke-test";
if(typeof __ENV.TEST_NAME !== 'undefined'){
	TEST_NAME = __ENV.TEST_NAME;
}

let INPUT_FILE = "./Resources/test-cases.json";
if(typeof __ENV.TEST_CASES_FILE !== 'undefined'){
	INPUT_FILE = __ENV.TEST_CASES_FILE;
}

//Load test cases from json file
const TEST_CASES= JSON.parse(open(INPUT_FILE));
		
//NSI-WS available data response types
const SUPPORTED_RESPONSE_FORMATS =["csv", "json", "xml"];
	
export let options = {
	ext: {
		loadimpact: {
		  projectID: 3497345, //k6 CLOUD project id 
		  name: TEST_NAME
		}
	},
	//Base line test 
	//Fixed number of iterations to execute the default function.
	iterations: 1,
	vus: 1,  // 1 user looping 
	//duration: '1m',
	thresholds: {
		'checks': ['rate>0.99'], // more than 99% success rate
		
        "http_req_duration{group:::Query type structure::Struc type agencyscheme}": ["p(95)<1000"],
        "http_req_duration{group:::Query type structure::Struc type categoryscheme}": ["p(95)<1000"],
        "http_req_duration{group:::Query type structure::Struc type categorisation}": ["p(95)<1000"],
        "http_req_duration{group:::Query type structure::Struc type codelist}": ["p(95)<1000"],
        "http_req_duration{group:::Query type structure::Struc type conceptscheme}": ["p(95)<1000"],
        "http_req_duration{group:::Query type structure::Struc type contentconstraint}": ["p(95)<1000"],
        "http_req_duration{group:::Query type structure::Struc type dataflow}": ["p(95)<1000"],
        "http_req_duration{group:::Query type structure::Struc type datastructure}": ["p(95)<1000"],		
        "http_req_duration{group:::Query type structure::Struc type hierarchicalcodelist}": ["p(95)<1000"],		
        "http_req_duration{group:::Query type structure::Struc type metadataflow}": ["p(95)<1000"],	
        "http_req_duration{group:::Query type structure::Struc type metadatastructure}": ["p(95)<1000"],	
        "http_req_duration{group:::Query type structure::Struc type structureset}": ["p(95)<1000"],	
				
        "http_req_duration{group:::Query type data::Format xml::Size extraSmall}": ["p(95)<8000"],
        "http_req_duration{group:::Query type data::Format xml::Size small}": ["p(95)<8000"],
        "http_req_duration{group:::Query type data::Format xml::Size medium}": ["p(95)<8000"],
        "http_req_duration{group:::Query type data::Format xml::Size large}": ["p(95)<15000"],
        "http_req_duration{group:::Query type data::Format xml::Size extraLarge}": ["p(95)<35000"],
		
        "http_req_duration{group:::Query type data::Format json::Size extraSmall}": ["p(95)<8000"],
        "http_req_duration{group:::Query type data::Format json::Size small}": ["p(95)<8000"],
        "http_req_duration{group:::Query type data::Format json::Size medium}": ["p(95)<8000"],
        "http_req_duration{group:::Query type data::Format json::Size large}": ["p(95)<10000"],
        "http_req_duration{group:::Query type data::Format json::Size extraLarge}": ["p(95)<30000"],
		
        "http_req_duration{group:::Query type data::Format csv::Size extraSmall}": ["p(95)<15000"],
        "http_req_duration{group:::Query type data::Format csv::Size small}": ["p(95)<8000"],
        "http_req_duration{group:::Query type data::Format csv::Size medium}": ["p(95)<8000"],
        "http_req_duration{group:::Query type data::Format csv::Size large}": ["p(95)<10000"],
        "http_req_duration{group:::Query type data::Format csv::Size extraLarge}": ["p(95)<45000"],
		
        "http_req_duration{datasetSize:extraSmall}": ["p(95)<15000"],
        "http_req_duration{datasetSize:small}": ["p(95)<15000"],
        "http_req_duration{datasetSize:medium}": ["p(95)<15000"],
        "http_req_duration{datasetSize:large}": ["p(95)<15000"],
        "http_req_duration{datasetSize:extraLarge}": ["p(95)<15000"],	
	},	
	//Discard the response bodies to lessen the amount of memmory required by the testing machine.
	discardResponseBodies: true,

};

export function setup() {
	// 2. setup code
		
	//check that the nsi-ws hostname is available
	let healhCheck = http.get(`${NSIWS_HOSTNAME}/health`);
	if (healhCheck.status !== 200){
		fail(`Error: the NSI-WS {${NSIWS_HOSTNAME}/health} is not responding.`);
	}
	
	console.log(`Testing the NSI-WS {${NSIWS_HOSTNAME}}`);
}

export default function() {
	//Iterate throu
	for(let testCase in TEST_CASES){
		//Generate random query 
		let testQuery = TEST_CASES[testCase];
		//console.log('Query: ', testQuery.query);
		
		let params = { headers: {'Accept-Ecoding': 'gzip, deflate'}	};
		
		group(`Query type ${testQuery.queryType}`, function (){
			
			//structure query			
			if(testQuery.queryType==="structure"){
				group(`Struc type ${testQuery.structureType}`, function (){
					let response = http.get(
						`${NSIWS_HOSTNAME}${testQuery.query}`, //URL
						params, //headers 
					);	
								
					check(response, {
						"status is 200": (r) => r.status == 200
					});
				});
			}
				
			//data query
			else{
				for(let format in SUPPORTED_RESPONSE_FORMATS){
					//Generate random format 
					let responseFormat = SUPPORTED_RESPONSE_FORMATS[format];
					if(typeof __ENV.RESPONSE_FORMAT !== 'undefined')
						responseFormat = __ENV.RESPONSE_FORMAT;
					//console.log('Format: ', responseFormat);
					
					params.headers.Accept=`application/vnd.sdmx.data+${responseFormat}`;
						
					if(responseFormat==="xml")
						params.headers.Accept=`application/vnd.sdmx.genericdata+${responseFormat}`;
					
					group(`Format ${responseFormat}`, function (){
						group(`Size ${testQuery.responseSize}`, function (){
							let response = http.get(
								`${NSIWS_HOSTNAME}${testQuery.query}`, //URL
								{tags:{datasetSize:testQuery.datasetSize}},//tags
								params, //headers 
							);	
								
							check(response, {
								"status is 200": (r) => r.status == 200
							});
						});
					});
					//wait 1 second after each test iteration
					sleep(1);
				}
			}
		});
	}
}
