# Nsi plugin
## NSI plugin Installation
[Installation document](docs/installation/CodeBaseApproach.md)

## NSI plugin Configuration
The NSI dotstat plugin configuration should be added to the file `/config/dataspaces.private.json` as a json node. 

### Keys
Name|Description|Type|Required
---|---|---|---
SpacesInternal|List of spaces ex. capture, stage, design, disseminate. See further information in table [SpacesInternal](# SpacesInternal)|DataspaceInternal|Yes
DotStatSuiteCoreCommonDbConnectionString|Connection string for the common database, used for authorization|string|Yes

#### SpacesInternal

Name|Description|Type|Required
---|---|---|---
Id|Unique identifier for the data space, ex. "design"|string|Yes
DotStatSuiteCoreStructDbConnectionString|Connection string for the mapping store database (structure database)|string|Yes
DotStatSuiteCoreDataDbConnectionString|Connection string for the data database|string|Yes
DataImportTimeOutInMinutes|Max. time in minutes for a data import transaction|int|Yes
DatabaseCommandTimeoutInSec|Max. time in seconds for a single SQL command execution|int|Yes
AutoLog2DB|Specify if the log4net logging configuration should be generated automatically to log into the Data DB|bool|No (Default false)
AllowPITTargetDataVersion|Specify if access to Point-in-Time data should be allowed. If false, attempts to do so result in a 401|bool|No (Default true)

### Example

The following is a basic configuration example for an NSI service, configured to serve one dataspace with id *design*
```json
{
  "DotStatSuiteCoreCommonDbConnectionString": "Data Source=localhost;Initial Catalog=COMMON_DB;User ID=USERNAME;Password=XXXXXX",
  "spacesInternal": [
    {
      "Id": "design",
      "DotStatSuiteCoreStructDbConnectionString": "Data Source=localhost;Initial Catalog=STRUCTURE_DB;User ID=USERNAME;Password=XXXXXX",
      "DotStatSuiteCoreDataDbConnectionString": "Data Source=localhost;Initial Catalog=DATA_DB;User ID=USERNAME;Password=XXXXXX",
      "DataImportTimeOutInMinutes": 60,
      "DatabaseCommandTimeoutInSec": 60
    }
  ]
}
```

## Register dotstat retriever
Inside the node "retrievers" inside "module", in the NSI webservice app.config, add the following line.
```xml
<module>
    <retrievers>
        ...
	    <add name="DotStatRetrieverFactory"/>	      
    </retrievers>
</module>
```

## Register MappingStoreServer
Inside the node "connectionStrings" inside "configuration", in the NSI webservice app.config, add the following line.
```xml
<configuration>
    <connectionStrings>
        ...
        <add name="MappingStoreServer" connectionString="Data Source=STRUCTURE_DB_HOST;Initial Catalog=STRUCTURE_DB;User ID=USERNAME;Password=XXXXXX;" providerName="System.Data.SqlClient"/>
    </connectionStrings>
</configuration>
```

### Authentication/Authorization

The prerequisites to setup Authentication/Authorization with NSI web service are following:

- OpenIdMiddlewareBuilder middleware is enabled in NSI main config file
- Auth configuration is defined (for example /config/auth.json)
- DotStatSuiteCoreCommonDbConnectionString connection string to a database with auth rules should be set as descibed in [this section](#keys)  
- Authorization rules are defined through [Authorization service](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management)

**NB!** Authorization currently is enforced only for data requests, for structure upload and retrieval no authorization checks are performed. 

To enable OpenID middleware set **OpenIdMiddlewareBuilder** under `<appSettings>` in `middlewareImplementation` of NSI webservice `app.config` file.

Example:

```xml
<add key="middlewareImplementation" value="OpenIdMiddlewareBuilder"/>
```

Auth configuration should be added to the file with a `json` extension inside a main `config` folder (for example /config/auth.json). Example configuration as a json object:

```json
{
  "auth": {
    "enabled": true,
    "allowAnonymous": false,
    "authority": "AUTHORITY URL",
    "clientId": "VALUE",
    "requireHttps": true,
    "validateIssuer": true,
    "showPii": false,
    "claimsMapping": {
        "email": "email",
        "groups": "groups"
    }
  }
}
```

| Setting    | Description |
|------------|-------------|
| enabled | Is openid authentication enabled|
| allowAnonymous | Is anonymous access allowed (request without JWT token)|
| authority | Authority url of token issuer |
| clientId | Client/application Id |
| requireHttps | Is HTTPS connection to OpenId authority server required |
| validateIssuer | Is iss (issuer) claim in JTW token should match configured authority |
| showPii | If set to TRUE outputs addition debug information in case of invalid token |
| claimsMapping | Key/value mapping of a key used in the C# code to a JWT token claim. |

### Logging configurarion
How to modify the default logging configuration? [See more.](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/blob/develop/docs/readme.md)