﻿using System;
using System.Data.Common;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Principal;
using DotStat.Common.Auth;
using DotStat.Domain;
using DotStat.NSI.DataRetriever;
using DotStat.NSI.DataRetriever.Helper;
using Estat.Sdmxsource.Extension.Manager;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Ws.Retrievers.Factory;
using Microsoft.AspNetCore.Http;
using Org.Sdmxsource.Sdmx.Api.Builder;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
using Org.Sdmxsource.Sdmx.DataParser.Manager;
using DotStat.Common.Logger;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Db.Manager;
using DotStat.Db.Repository;
using DotStat.MappingStore;
using DryIoc;
using Microsoft.Extensions.Configuration;
using Org.Sdmxsource.Util;

namespace DotStat.NSI.RetrieverFactory.Factory
{
    /// <summary>
    /// The DotStat implementation of retriever plugin.
    /// </summary>
    public class DotStatRetrieverFactory : SriRetrieverFactory
    {
        private readonly DryIoc.Container _container;
        private readonly IHeaderRetrievalManager _defaultHeaderRetrievalManager;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly bool _allowPITTargetDataVersion;
        private readonly string _spaceId;

        /// <summary>
        /// Gets the unique identifier of the implementation.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public override string Id => "DotStatRetrieverFactory";

        /// <summary>
        /// Initializes a new instance of the <see cref="SriRetrieverFactory" /> class.
        /// </summary>
        /// <param name="contextAccessor">The http context accessor.</param>
        /// <param name="defaultHeaderRetrievalManager">The default header retrieval manager.</param>
        /// <param name="mappingStoreConnectionStringBuilder">The mappingStore connection string builder.</param>
        /// <param name="connectionBuilder">The connection builder.</param>
        /// <param name="mappingManager">The mapping manager.</param>
        /// <param name="mappingValidationManager">The mapping validation manager.</param>
        /// <param name="dataflowPrincipalManager"></param>
        /// <param name="configuration"></param>
        /// <exception cref="ArgumentNullException">
        /// defaultHeaderRetrievalManager
        /// or
        /// configManager
        /// or
        /// connectionBuilder
        /// or
        /// mappingManager
        /// or
        /// mappingValidationManager
        /// </exception>
        /// <exception cref="SdmxException">Could not establish a connection to the mapping store DB</exception>
        public DotStatRetrieverFactory(
            IHttpContextAccessor contextAccessor,
            IHeaderRetrievalManager defaultHeaderRetrievalManager,
            IConnectionStringBuilderManager mappingStoreConnectionStringBuilder,
            IBuilder<DbConnection, DdbConnectionEntity> connectionBuilder,
            IComponentMappingManager mappingManager,
            IComponentMappingValidationManager mappingValidationManager,
            IDataflowPrincipalManager dataflowPrincipalManager,
            IConfiguration configuration)
             : base(defaultHeaderRetrievalManager,
                   mappingStoreConnectionStringBuilder,
                   connectionBuilder,
                   mappingManager,
                   mappingValidationManager,
                   dataflowPrincipalManager,
                   configuration)
        {
            Log.Configure(contextAccessor);
            Log.Debug("Constructor"); 

            _contextAccessor = contextAccessor;
            _defaultHeaderRetrievalManager = defaultHeaderRetrievalManager;

            _container = DotStatDataRetrieverIoc.CreateContainer(configuration);

            var dataspaceConfiguration = _container.Resolve<IDataspaceConfiguration>();

            if(!dataspaceConfiguration.SpacesInternal.Any())
                throw new InvalidOperationException("NSI plugin dataspaces are not configured");

            var dataspace = dataspaceConfiguration.SpacesInternal.First();

            _spaceId = dataspace.Id;
            _allowPITTargetDataVersion = dataspace.AllowPITTargetDataVersion;
        }

        /// <summary>
        /// Gets the data retrieval which is responsible for retrieving data for REST (any version) and SOAP v2.0.
        /// This method is called once for every data request. 
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <param name="principal">The principal.</param>
        /// <returns>The <see cref="ISdmxDataRetrievalWithWriter" />.</returns>
        public override ISdmxDataRetrievalWithWriter GetDataRetrieval(SdmxSchema sdmxSchema, IPrincipal principal)
        {
            var acceptHeaderOfRequest = _contextAccessor.HttpContext.Request.Headers["Accept"];
            var releaseHeaderOfRequest = _contextAccessor.HttpContext.Request.Headers["X-DotStat-Release"];
            
            var targetVersion = releaseHeaderOfRequest.Any(s => s.Equals("PIT", StringComparison.InvariantCultureIgnoreCase)) 
                ? TargetVersion.PointInTime 
                : TargetVersion.Live;

            if(!_allowPITTargetDataVersion && targetVersion==TargetVersion.PointInTime)
                throw new SdmxUnauthorisedException("Point in Time access disabled.");

            Log.Debug($"Data request: {acceptHeaderOfRequest}");
                
            var retriever = new DotStatDataRetrieverCore(
                _contextAccessor.HttpContext.User,
                _defaultHeaderRetrievalManager.Header,
                base.GetDataRetrieval(sdmxSchema, principal),
                _container.Resolve<IDbManager>(),
                _container.Resolve<IMappingStoreDataAccess>(),
                _container.Resolve<IObservationRepository>(),
                _container.Resolve<IAttributeRepository>(),
                _container.Resolve<IAuthorizationManagement>(),
                _container.Resolve<IAuthConfiguration>(),
                HeaderScope.Range,
                acceptHeaderOfRequest,
                targetVersion,
                _spaceId
            );

            // GetNumberOfObservations is only called when Range header is present
            if (HeaderScope.Range != null)
            {
                _contextAccessor.HttpContext.Items["retriever"] = retriever;
            }

            return retriever;
        }

        /// <summary>
        /// Gets the advanced data retrieval which is responsible for retrieving data using the advanced SDMX v2.1 SOAP queries.
        /// </summary>
        /// <param name="principal">The principal.</param>
        /// <returns>The <see cref="IAdvancedSdmxDataRetrievalWithWriter" />.</returns>
        public override IAdvancedSdmxDataRetrievalWithWriter GetAdvancedDataRetrieval(IPrincipal principal)
        {
            throw new SdmxNotImplementedException();
        }

        /// <summary>
        /// Gets the data retrieval with cross which is responsible for retrieving SDMX v2.0 Cross Sectional data for REST (any version) and SOAP v2.0.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <param name="principal">The principal.</param>
        /// <returns>The <see cref="ISdmxDataRetrievalWithCrossWriter" />.</returns>
        public override ISdmxDataRetrievalWithCrossWriter GetDataRetrievalWithCross(SdmxSchema sdmxSchema, IPrincipal principal)
        {
            throw new SdmxNotImplementedException();
        }

        /// <summary>
        /// This method called once per Data request and before calling ISdmxDataRetrievalWithWriter.GetData
        /// </summary>
        /// <param name="dataQuery"></param>
        /// <returns></returns>
        [ExcludeFromCodeCoverage]
        public override int GetNumberOfObservations(IDataQuery dataQuery)
        {
            // Number of observation comes together with main data query, we try to reuse structural information between count & data calls
            var retriever =_contextAccessor.HttpContext.Items["retriever"] as DotStatDataRetrieverCore;

            if(retriever == null)
                throw new InvalidOperationException();

            return retriever.GetNumberOfObservations(dataQuery);
        }
    }
}