﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using DotStat.Common.Auth;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Logger;
using DotStat.Db;
using DotStat.Db.Exception;
using DotStat.Db.Manager;
using DotStat.Db.Repository;
using DotStat.Db.Util;
using DotStat.Domain;
using DotStat.MappingStore;
using Estat.Nsi.DataRetriever;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
using Org.Sdmxsource.Sdmx.Api.Model.Header;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

namespace DotStat.NSI.DataRetriever
{
    /// <summary>
    /// Not a thread safe class, should be used with HTTP request scope
    /// </summary>
    public class DotStatDataRetrieverCore : ISdmxDataRetrievalWithWriter
    {
        private readonly IHeader _defaultHeader;
        private readonly Tuple<long?, long?> _rangeScope;
        private readonly string _acceptHeaderOfRequest;
        private readonly string _dataSpace;
        private readonly IObservationRepository _observationRepository;
        private readonly ISdmxDataRetrievalWithWriter _sriDataRetriever;
        private readonly IMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly IAttributeRepository _attributeRepository;
        private readonly IAuthConfiguration _authConfiguration;
        private readonly IAuthorizationManagement _authorizationManagement;
        private readonly IManagementRepository _managementRepository;
        private readonly DotStatPrincipal _principal;
        private const string NaN = "NaN";
        private readonly TargetVersion _targetVersion;

        private Dataflow _dataflow;
        private ICodeTranslator _codeTranslator;

        public DotStatDataRetrieverCore(
            IPrincipal principal,
            IHeader defaultHeader,
            ISdmxDataRetrievalWithWriter sriDataRetriever,
            IDbManager dbManager,
            IMappingStoreDataAccess mappingStoreDataAccess,
            IObservationRepository observationRepository,
            IAttributeRepository attributeRepository,
            IAuthorizationManagement authorizationManagement,
            IAuthConfiguration authConfiguration,
            Tuple<long?, long?> rangeScope,
            string acceptHeaderOfRequest,
            TargetVersion? targetVersion,
            string dataSpace
        )
        {
            _defaultHeader = defaultHeader ?? throw new ArgumentNullException(nameof(defaultHeader));
            
            _rangeScope = rangeScope;
            _acceptHeaderOfRequest = acceptHeaderOfRequest;
            _targetVersion = targetVersion ?? TargetVersion.Live;
            _sriDataRetriever = sriDataRetriever;

            _dataSpace = dataSpace;
            _managementRepository = dbManager.GetManagementRepository(dataSpace);
            _mappingStoreDataAccess = mappingStoreDataAccess;
            _observationRepository = observationRepository;
            _attributeRepository = attributeRepository;
            _authorizationManagement = authorizationManagement;
            _authConfiguration = authConfiguration;

            _principal = new DotStatPrincipal(principal as ClaimsPrincipal ?? new ClaimsPrincipal(), _authConfiguration.ClaimsMapping);

            Log.SetDataspaceId(_dataSpace);
        }

        public void GetData(IDataQuery dataQuery, IDataWriterEngine dataWriter)
        {
            //Check if dataflow has attached MappingSet, if true then query data using the default data retriever, if not the us the DotstatV8 data retriever
            if (_mappingStoreDataAccess.HasMappingSet(dataQuery.Dataflow))
                _sriDataRetriever.GetData(dataQuery, dataWriter);
            else
                GetDotStatV8GetData(dataQuery, dataWriter);
        }

        private string WriteObservation(
            IDataWriterEngine dataWriter, 
            IObservation observation, 
            string previousKey, 
            string obsDimension,
            DimAttributeRetriever dimensionAttributeRetriever, 
            bool writeNaNForNullObservationValues
        )
        {
            var seriesKey = observation.SeriesKey;

            var currentKey = seriesKey.GetKey();

            if (!currentKey.Equals(previousKey))
            {
                // Start series
                dataWriter.StartSeries();

                // Series key
                foreach (var dim in seriesKey.Key)
                {
                    dataWriter.WriteSeriesKeyValue(dim.Concept, dim.Code);
                }

                foreach (var attributeKeyable in dimensionAttributeRetriever.GetSeriesAttributes(seriesKey))
                {
                    foreach (var dimensionAttribute in attributeKeyable.Attributes.Where(x=>!string.IsNullOrEmpty(x.Code)))
                    {
                        dataWriter.WriteAttributeValue(dimensionAttribute.Concept, dimensionAttribute.Code);
                    }
                }
            }

            dataWriter.WriteObservation(obsDimension, observation.ObsTime,
                string.IsNullOrEmpty(observation.ObservationValue) && writeNaNForNullObservationValues
                    ? NaN
                    : observation.ObservationValue);

            // Observation level attributes
            if (observation.Attributes != null)
            {
                foreach (var attrValue in observation.Attributes.Where(x=>!string.IsNullOrEmpty(x.Code)))
                {
                    dataWriter.WriteAttributeValue(attrValue.Concept, attrValue.Code);
                }
            }

            return currentKey;
        }

        public int GetNumberOfObservations(IDataQuery dataQuery)
        {
            try
            {
                this.InitDataflow(dataQuery);

                return _observationRepository.GetObservationsCount(dataQuery, _dataflow, _codeTranslator, _dataSpace, _targetVersion);
            }
            catch (ArtefactNotFoundException ex)
            {
                return 0;
            }
        }

        private void ValidateAuthorization(IDataflowObject dataflow)
        {
            if (!_authConfiguration.Enabled)
                return;

            var isAuthorized = _authorizationManagement.IsAuthorized(
                _principal,
                _dataSpace,
                dataflow.AgencyId,
                dataflow.Id,
                dataflow.Version,
                PermissionType.CanReadData
            );

            if (!isAuthorized)
                throw new DataRetrieverException("Unauthorised", SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.Unauthorised));
        }

        private void GetDotStatV8GetData(IDataQuery dataQuery, IDataWriterEngine dataWriterEngine)
        {
            if (dataQuery == null)
            {
                throw new ArgumentNullException(nameof(dataQuery));
            }

            if (dataWriterEngine == null)
            {
                throw new ArgumentNullException(nameof(dataWriterEngine));
            }

            try
            {
                ValidateAuthorization(dataQuery.Dataflow);

                var rangeStart = _rangeScope?.Item1;
                if (rangeStart.HasValue)
                {
                    if (rangeStart != 0)
                    {
                        throw new SdmxNotImplementedException("Range start not equal to 0.");
                    }

                    rangeStart++; // Range header contains 0-bases indexes
                }

                var rangeEnd = _rangeScope?.Item2;
                if (rangeEnd.HasValue)
                {
                    rangeEnd++; // Range header contains 0-bases indexes
                }

                this.InitDataflow(dataQuery);

                using (var observationEnumerator = _observationRepository.GetObservations(dataQuery, _dataflow, _codeTranslator, _dataSpace, _targetVersion, rangeStart, rangeEnd).GetEnumerator())
                {
                    if (!observationEnumerator.MoveNext())
                    {
                        throw new SdmxNoResultsException("NoRecordsFound");
                    }
                    Log.Debug($"Range: {_rangeScope?.Item1}/{_rangeScope?.Item2}");

                    var isJsonOrCsv = !string.IsNullOrWhiteSpace(_acceptHeaderOfRequest) &&
                                  (_acceptHeaderOfRequest.Contains("json") || _acceptHeaderOfRequest.Contains("csv"));

                    var isAllDimensionsAtObservationRequested = dataQuery.DimensionAtObservation.Equals(DatasetStructureReference.AllDimensions,
                        StringComparison.OrdinalIgnoreCase);

                    var specialTreatmentForGroupAttributes = isJsonOrCsv || isAllDimensionsAtObservationRequested;

                    dataWriterEngine.WriteHeader(_defaultHeader);

                    var dsr = new DatasetStructureReferenceCore(string.Empty,
                        dataQuery.DataStructure.AsReference, null, null, dataQuery.DimensionAtObservation);

                    var header = new DatasetHeaderCore(dataQuery.DataStructure.Id, DatasetActionEnumType.Information, dsr, SdmxStructureEnumType.Dataflow);

                    dataWriterEngine.StartDataset(dataQuery.Dataflow, dataQuery.DataStructure, header);
                    
                    // Dataset level attributes
                    foreach (var datasetAttributeKeyValue in _attributeRepository.GetDatasetAttributes(dataQuery, _dataflow, _codeTranslator, _dataSpace, _targetVersion))
                    {
                        dataWriterEngine.WriteAttributeValue(datasetAttributeKeyValue.Concept, datasetAttributeKeyValue.Code);
                    }

                    var dimensionAttributeRetriever = new DimAttributeRetriever();

                    foreach (var attributeKeyable in _attributeRepository.GetKeyables(dataQuery, _dataflow, _codeTranslator, _dataSpace, _targetVersion))
                    {
                        if (attributeKeyable.Series)
                        {
                            // Dimension level attributes are collected to be able to present them at series in output file
                            dimensionAttributeRetriever.Add(attributeKeyable);
                            continue;
                        }

                        if (specialTreatmentForGroupAttributes)
                        {
                            // Workaround for JSON writer where existence of group name prevents writing keyable
                            // If needed group level attributes are collected to be able to present them at series in output file
                            var attributeKeyableWithoutGroupName = new KeyableImpl(attributeKeyable.Dataflow,
                                attributeKeyable.DataStructure, attributeKeyable.Key,
                                attributeKeyable.Attributes, attributeKeyable.TimeFormat, attributeKeyable.Annotations.ToArray());

                            dimensionAttributeRetriever.Add(attributeKeyableWithoutGroupName);
                            continue;
                        }

                        // When no special treatment is need for group level attributes they are immediately written (e.g. SDMX-ML series format)
                        dataWriterEngine.StartGroup(attributeKeyable.GroupName);

                        foreach (var dimKeyValue in attributeKeyable.Key)
                        {
                            dataWriterEngine.WriteGroupKeyValue(dimKeyValue.Concept, dimKeyValue.Code);
                        }

                        // Group level attributes
                        foreach (var attributeKeyValue in attributeKeyable.Attributes)
                        {
                            dataWriterEngine.WriteAttributeValue(attributeKeyValue.Concept, attributeKeyValue.Code);
                        }
                    }

                    var obsDimension = isAllDimensionsAtObservationRequested
                        ? dataQuery.DataStructure.TimeDimension?.Id
                        : dataQuery.DimensionAtObservation;

                    var previousKey = "";

                    bool.TryParse(ConfigurationManager.AppSettings["WriteNaNForNullObservationValues"], out bool writeNaNForNullObservationValues);

                    // Use only for sdmx-ml
                    writeNaNForNullObservationValues = !isJsonOrCsv && writeNaNForNullObservationValues;

                    // Write observation - enumerator points already to the first observation
                    do
                    {
                        previousKey = WriteObservation(
                            dataWriterEngine, 
                            observationEnumerator.Current, 
                            previousKey, 
                            obsDimension,
                            dimensionAttributeRetriever, 
                            writeNaNForNullObservationValues
                        );
                    } 
                    while (observationEnumerator.MoveNext());
                }
            }
            catch (ArtefactNotFoundException)
            {
                throw new SdmxNoResultsException("No data found");
            }
            catch (SdmxResponseSizeExceedsLimitException e)
            {
                dataWriterEngine.Close(
                    new FooterMessageCore(e.SdmxErrorCode.ClientErrorCode.ToString(CultureInfo.InvariantCulture),
                        Severity.Error,
                        new TextTypeWrapperImpl("en", e.SdmxErrorCode.ErrorString + " : " + e.Message, null)));
                throw;
            }
            catch (SdmxResponseTooLargeException e)
            {
                dataWriterEngine.Close(
                    new FooterMessageCore(e.SdmxErrorCode.ClientErrorCode.ToString(CultureInfo.InvariantCulture),
                        Severity.Error,
                        new TextTypeWrapperImpl("en", e.SdmxErrorCode.ErrorString + " : " + e.Message, null)));
                throw;
            }
            catch (DataRetrieverException)
            {
                throw;
            }
            catch (SdmxException)
            {
                throw;
            }
            catch (DbException ex)
            {
                throw new DataRetrieverException(ex,
                    SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError),
                    "Error executing generated SQL and populating SDMX model");
            }
            catch (OutOfMemoryException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DataRetrieverException(ex,
                    SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError),
                    "Error during writing response");
            }
            finally
            {
                dataWriterEngine.Close();
            }
        }

        private void InitDataflow(IDataQuery dataQuery)
        {
            if (_dataflow != null)
                return;
            
            _dataflow = _mappingStoreDataAccess.GetDataflow(
                _dataSpace,
                dataQuery.Dataflow.AgencyId,
                dataQuery.Dataflow.Id,
                dataQuery.Dataflow.Version
            );
            _managementRepository.FillIdsFromDissemenationDb(_dataflow);

            _codeTranslator = new CodeTranslator(_managementRepository);
            _codeTranslator.FillDict(_dataflow);
        }

        private class DimAttributeRetriever
        {
            private readonly Dictionary<string, DimAttributeBag> _dict = new Dictionary<string, DimAttributeBag>();

            public void Add(IKeyable key)
            {
                var dims = key.Key.Select(x => x.Concept).ToArray();
                var signature = string.Join(':', dims);

                if (!_dict.TryGetValue(signature, out var bag))
                {
                    _dict[signature] = bag = new DimAttributeBag(dims);
                }
                
                bag.Add(key);
            }

            public IEnumerable<IKeyable> GetSeriesAttributes(IKeyable series)
            {
                foreach (var bag in _dict.Values)
                {
                    var result = bag.Fetch(series);
                    
                    if (result != null)
                        yield return result;
                }
            }

            private class DimAttributeBag
            {
                private readonly IList<string> _key;
                private readonly Dictionary<string, IKeyable> _dict;

                public DimAttributeBag(IList<string> key)
                {
                    _key = key;
                    _dict = new Dictionary<string, IKeyable>();
                }

                public void Add(IKeyable attribute) => _dict[String.Join(":", attribute.Key.Select(x => x.Code))] = attribute;

                public IKeyable Fetch(IKeyable series)
                {
                    var key = string.Join(":", _key.Select(series.GetKeyValue));

                    return _dict.TryGetValue(key, out var result) ? result : null;
                }
            }
        }
        
    }
}