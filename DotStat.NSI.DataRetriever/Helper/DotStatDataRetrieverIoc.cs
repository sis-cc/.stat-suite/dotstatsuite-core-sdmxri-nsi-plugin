﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db.Manager;
using DotStat.Db.Manager.SqlServer;
using DotStat.Db.Repository;
using DotStat.Db.Repository.SqlServer;
using DotStat.MappingStore;
using DryIoc;
using log4net;
using Microsoft.Extensions.Configuration;

namespace DotStat.NSI.DataRetriever.Helper
{
    [ExcludeFromCodeCoverage]
    public static class DotStatDataRetrieverIoc
    {
        private static AuthConfiguration DefaultAuthConfiguration()
        {
            return new AuthConfiguration()
            {
                ClaimsMapping = new Dictionary<string, string>()
            };
        }

        public static Container CreateContainer(IConfiguration configuration)
        {
            var container = new Container();

            SetupDotStatConfiguration(container, configuration);

            container.Register<IDbManager, SqlServerDbManager>(Reuse.Singleton);
            container.Register<IMappingStoreDataAccess, MappingStoreDataAccess>(Reuse.Transient);
            container.Register<IObservationRepository, SqlObservationRepository>(Reuse.Singleton);
            container.Register<IAttributeRepository, SqlAttributeRepository>(Reuse.Singleton);
            container.Register<IAuthorizationRepository, SqlAuthorizationRepository>(Reuse.Singleton);
            container.Register<IAuthorizationManagement, AuthorizationManagement>(Reuse.Singleton);

            return container;
        }
        
        public static void SetupDotStatConfiguration(IContainer container, IConfiguration configuration)
        {
            var authConfig = configuration.GetSection("auth").Get<AuthConfiguration>() ?? DefaultAuthConfiguration();
            var baseConfig = configuration.Get<BaseConfiguration>();

            // ----------------------------------------------------------

            container.UseInstance(baseConfig);
            container.UseInstance<IDataspaceConfiguration>(baseConfig);
            container.UseInstance<ILocalizationConfiguration>(baseConfig);
            container.UseInstance<IMailConfiguration>(baseConfig);
            container.UseInstance<IGeneralConfiguration>(baseConfig);
            container.UseInstance<IAuthConfiguration>(authConfig);

            LocalizationRepository.Configure(baseConfig);

            //Log4Net 
            const string applicationName = "sdmxri-nsi-ws";
            GlobalContext.Properties[CustomParameter.Application.ToString()] = applicationName;

            LogHelper.ConfigureAppenders(baseConfig);

            Log.Info($"{applicationName} application configured. Version number: {Assembly.GetExecutingAssembly().GetName().Version}");
            Log.Info($"Dataspaces defined in configuration: [{string.Join(',', baseConfig.SpacesInternal.Select(s => s.Id))}]");
        }
    }
}